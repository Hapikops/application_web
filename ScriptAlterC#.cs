#region  ajout des champs Latitude et Longitude 
        public void addChampsLatitude()
        {
            // sqlQuery 
            string sqlQuery = "ALTER TABLE tblEtatActuelK7 ADD Latitude DOUBLE";
           OleDbCommand command = new OleDbCommand(sqlQuery, oleDbConnection_BCP_LP);
           command.ExecuteNonQuery(); 
            

        }

        public void addChampsLongitude()
        {
            // sqlQuery
            string sqlQuery = "ALTER TABLE tblEtatActuelK7 ADD Longitude DOUBLE";
            OleDbCommand command = new OleDbCommand(sqlQuery, oleDbConnection_BCP_LP);
            command.ExecuteNonQuery(); 
            
        }
#endregion