﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

///
/// Auteur : Burléon Junior
/// Date de création : 03/02/2014
/// 
/// Attention à bien lire le commentaire de chaque attribut. Car un attribut ne porte pas toujours le même nom que celui qu'il porte en database.
/// La raison de cela est pour donner des noms plus cohérant et plus parlant que ceux utilisé en database.
///

namespace BiocoldProcess.Domaine
{
    /// <summary>
    /// Classe définissant un utilisateur.
    /// </summary>
    [DataContract(Namespace = "http://extranetbcp.fr/WS/Domaine/Utilisateur")]
    public class Utilisateur
    {
        /// <summary>
        /// Id de l'utilisateur
        /// 
        /// Dans la db : id_util
        /// </summary>
        private int id;
        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Login est en réalité le nom dans la table tblUtilisateur
        /// Il doit également être unique car c'est grâce à lui que l'on se connecte sur l'intranet
        /// 
        /// Dans la db : nom_util
        /// </summary>
        private string login;
        [DataMember]
        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        /// <summary>
        /// Mot de passe de l'utilisateur
        /// 
        /// Dans la db : mopass_util
        /// </summary>
        private string password;
        [DataMember]
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        /// <summary>
        /// NiveauAcces d'accès au site 
        /// 1 : Admin
        /// 2 : ComptaAdm
        /// 3 : SecretAdm
        /// 4 : LogistAdm
        /// 5 : Partenaire
        /// 6 : AgentPart
        /// 7 : SecretPart
        /// 8 : ???
        /// 9 : PartCoach
        /// 10 : Logist ASI
        /// 11 : AgentPArt & Logistique ASI (PF)
        /// 12 : Stagiaire BTS AC & FV
        /// 20 : CNE
        /// 30 : TELEOP
        /// 
        /// Dans la db : niveau_util
        /// </summary>
        private int niveauAcces;
        [DataMember]
        public int NiveauAcces
        {
            get { return niveauAcces; }
            set { niveauAcces = value; }
        }

        /// <summary>
        /// DateSaisie de création du compte utilisateur
        /// 
        /// Dans la db : date_creation_util
        /// </summary>
        private DateTime dateCreation;
        [DataMember]
        public DateTime DateCreation
        {
            get { return dateCreation; }
            set { dateCreation = value; }
        }

        /// <summary>
        /// Idententifiant du partenaire auquel serait lié l'utilisateur
        /// 
        /// Dans la db : id_part
        /// </summary>
        private string idPartenaire;
        [DataMember]
        public string IdPartenaire
        {
            get { return idPartenaire; }
            set { idPartenaire = value; }
        }

        /// <summary>
        /// Le site est t'il en maintenance ? Oui / Non
        /// 
        /// Dans la db : maintenance
        /// </summary>
        private bool maintenance;
        
        public bool Maintenance
        {
            get { return maintenance; }
            set { maintenance = value; }
        }
    }
}