﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

///
/// Auteur : Burléon Junior
/// Date de création : 04/02/2014
/// 

namespace BiocoldProcess.Domaine
{
    /// <summary>
    /// Cette classe permet de représenté le scan d'une cassette.
    /// Cette à dire tout les informations récupérée lors du scan d'un code barre sur une cassette.
    /// </summary>
    [DataContract(Namespace = "http://extranetbcp.fr/WS/Domaine/ScanK7")]
    public class ScanK7
    {
        /// <summary>
        /// Identifiant d'un scan
        /// 
        /// Dans la db : numautoEtatActuelK7
        /// </summary>
        private int id;
        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Le code en lui même du code barre se trouvant sur chaque cassette (exemple : K0000001)
        /// 
        /// Dans la db : numK7
        /// </summary>
        private string code;
        [DataMember]
        public string Code
        {
            get { return code; }
            set { code = value; }
        }

        /// <summary>
        /// ID de l'utilisateur qui fait le scan.
        /// Cette ID permettra de retrouver quel Partenaire précisément à fait le scan.
        /// 
        /// Dans la db : idpart
        /// </summary>
        private int idUser;
        [DataMember]
        public int IdUser
        {
            get { return idUser; }
            set { idUser = value; }
        }

        /// <summary>
        /// Permet de positionner le smartphone lors du scan.
        /// La longitude du smartphone.
        /// 
        /// Dans la db : Longitude
        /// </summary>
        private double longitude;
        [DataMember]
        public double Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }

        /// <summary>
        /// Permet de positionner le smartphone lors du scan.
        /// La latitude du smartphone.
        /// 
        /// Dans la db : Latitude
        /// </summary>
        private double latitude;
        [DataMember]
        public double Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }

        /// <summary>
        /// Date et heure à laquelle est scanné le code barre 
        /// OU 
        /// Date et heure à laquel les informations sont entré dans le système (A la main, et donc peut-être en différé par rapport au moment du changement de cassette).
        /// 
        /// Dans la db : dateSaisie
        /// </summary>
        private DateTime dateSaisie;
        [DataMember]
        public DateTime DateSaisie
        {
            get { return dateSaisie; }
            set { dateSaisie = value; }
        }

        /// <summary>
        /// ID du passage de l'agent chez le client.
        /// Pour permettre donc un lien entre le passage chez le client et toutes les K7 qui y sont scannées.
        /// 
        /// Dans la db : NumChgt
        /// </summary>
        private int idPassageAgent;
        [DataMember]
        public int IdPassageAgent
        {
            get { return idPassageAgent; }
            set { idPassageAgent = value; }
        }

        /// <summary>
        /// Etat de la cassette.
        /// Cette attribut correspond à l'id d'un état dans la table : tblListeEtatK7 qui est dans la db BCP_LP
        /// 
        /// Dans la db : etatK7
        /// </summary>
        private int etatK7;
        [DataMember]
        public int EtatK7
        {
            get { return etatK7; }
            set { etatK7 = value; }
        }

        /// <summary>
        /// Date à laquel le dernier changement d'état à eu lieu.
        /// 
        /// Dans la db : datedepartEtatK7
        /// </summary>
        private DateTime etatDate;
        [DataMember]
        public DateTime EtatDate
        {
            get { return etatDate; }
            set { etatDate = value; }
        }

        /// <summary>
        /// ID de l'essai ou du contrat (étant donné que dans la db ils sont +- identique) auquel est lié la cassette qui est scannée.
        /// Si vous avez du mal à comprendre. Demandé à Jean-Marc de vous expliquez.
        /// 
        /// Dans la db : numess
        /// </summary>
        private int idEssai;
        [DataMember]
        public int IdEssai
        {
            get { return idEssai; }
            set { idEssai = value; }
        }
    }
}
