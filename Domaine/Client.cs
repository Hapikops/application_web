﻿using BiocoldProcess.Domaine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

///
/// Auteur : Burléon Junior
/// Date de création : 03/02/2014
/// 
/// Attention à bien lire le commentaire de chaque attribut. Car un attribut ne porte pas toujours le même nom que celui qu'il porte en database.
/// La raison de cela est de donner des noms plus cohérant et plus parlant que ceux utilisé en database.
///

namespace BiocoldProcess.Domaine
{
    /// <summary>
    /// Classe définissant un client.
    /// Attention : ici dans le projet il la classe client est une classe bien distincte mais en
    /// base de donnée elle se trouve persistée dans la table tblessaicontrat qui comme son nom l'indique, regroupe d'autre information que juste celle du client.
    /// </summary>
    [DataContract(Namespace = "http://extranetbcp.fr/WS/Domaine/Client")]
    public class Client
    {
        /// <summary>
        /// ID du client, est dans la base de donnée le N°Essai de la table tblessaicontrat
        /// 
        /// Dans la db : N°Essai
        /// </summary>
        private int id;
        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Nom du commerce du client
        /// 
        /// Dans la db : NomDuCommerce
        /// </summary>
        private string nomCommerce;
        [DataMember]
        public string NomCommerce
        {
            get { return nomCommerce; }
            set { nomCommerce = value; }
        }

        /// <summary>
        /// Nom du client
        /// 
        /// Dans la db : NomClientEssai
        /// </summary>
        private string nom;
        [DataMember]
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        /// <summary>
        /// Prénom du client
        /// 
        /// Dans la db : PrenomClientEssai
        /// </summary>
        private string prenom;
        [DataMember]
        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }

        /// <summary>
        /// Adresse du commerce du client
        /// 
        /// Dans la db : AdresseEssai
        /// </summary>
        private string adresseCommerce;
        [DataMember]
        public string AdresseCommerce
        {
            get { return adresseCommerce; }
            set { adresseCommerce = value; }
        }

        /// <summary>
        /// Ville où est le commerce du client
        /// 
        /// Dans la db : VilleEssai
        /// </summary>
        private string villeCommerce;
        [DataMember]
        public string VilleCommerce
        {
            get { return villeCommerce; }
            set { villeCommerce = value; }
        }

        /// <summary>
        /// Code postale du commerce du client
        /// 
        /// Dans la db : CodePostalEssai
        /// </summary>
        private string codePostalCommerce;
        [DataMember]
        public string CodePostalCommerce
        {
            get { return codePostalCommerce; }
            set { codePostalCommerce = value; }
        }

        /// <summary>
        /// ID du contrat auquel est lié le client.
        /// Attention il peut-être vide.
        /// 
        /// Dans la db : InitialesPartenaire + NumContrat
        /// </summary>
        private string idContrat;
        [DataMember]
        public string IdContrat
        {
            get { return idContrat; }
            set { idContrat = value; }
        }

        /// <summary>
        /// ID du partenaire auquel est lié le client.
        /// 
        /// Dans la db : InitialesPartenaire
        /// </summary>
        private string idPartenaire;
        [DataMember]
        public string IdPartenaire
        {
            get { return idPartenaire; }
            set { idPartenaire = value; }
        }

        /// <summary>
        /// ID du partenaire auquel est lié le client.
        /// 
        /// 1 : M
        /// 2 : Mme  
        /// 3 : Mlle
        /// 
        /// Dans la db : IntituleClientEssai
        /// </summary>
        private int civilite;
        [DataMember]
        public int Civilite
        {
            get { return civilite; }
            set { civilite = value; }
        }

        /// <summary>
        /// ID de l'essai auquel est lié le client.
        /// 
        /// Dans la db : N°Essai
        /// </summary>
        private int idEssai;
        [DataMember]
        public int IdEssai
        {
            get { return idEssai; }
            set { idEssai = value; }
        }

        /// <summary>
        /// Téléphone du client.
        /// 
        /// Dans la db : 
        /// </summary>
        private int tel;
        [DataMember]
        public int Tel
        {
            get { return tel; }
            set { tel = value; }
        }

        /// <summary>
        /// Type de commerce du client.
        /// 
        /// </summary>
        private TypeCommerce typeCommerce;
        [DataMember]
        public TypeCommerce TypeCommerce
        {
            get { return typeCommerce; }
            set { typeCommerce = value; }
        }
    }
}