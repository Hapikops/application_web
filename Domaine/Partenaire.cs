﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

///
/// Auteur : Burléon Junior
/// Date de création : 30/01/2014
/// 
/// Attention à bien lire le commentaire de chaque attribut. Car un attribut ne porte pas toujours le même nom que celui qu'il porte en database.
/// La raison de cela est pour donner des noms plus cohérant et plus parlant que ceux utilisé en database.
///

namespace BiocoldProcess.Domaine
{
    /// <summary>
    /// Classe définissant un partenaire.
    /// Il n'y a pas autant de propriété que le permet la database mais n'hésitez pas à en ajouter au besoin.
    /// Mais attention Si vous en ajouté n'oublié pas de modifié et commenter ces modifications dans la classe PartenaireDao
    /// <seealso cref="DaoImpl.PartenaireDao"/>
    /// </summary>
    [DataContract(Namespace = "http://extranetbcp.fr/WS/Domaine/Partenaire")]
    public class Partenaire
    {
        /// <summary>
        /// ID du patenraire.
        /// 
        /// Dans la db : InitialesPartenaire
        /// </summary>
        private string id;
        [DataMember]
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Nom du partenaire
        /// 
        /// Dans la db : NomPartenaire
        /// </summary>
        private string nom;
        [DataMember]
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        /// <summary>
        /// Prénom
        /// 
        /// Dans la db : PrénomPartenaire
        /// </summary>
        private string prenom;
        [DataMember]
        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }

        /// <summary>
        /// Adresse (Exemple : Rue du mort 24)
        /// 
        /// Dans la db : AdrPart
        /// </summary>
        private string adresse;
        [DataMember]
        public string Adresse
        {
            get { return adresse; }
            set { adresse = value; }
        }

        /// <summary>
        /// Code Postale 
        /// 
        /// Dans la db : CodePostPart
        /// </summary>
        private string codePostal;
        [DataMember]
        public string CodePostal
        {
            get { return codePostal; }
            set { codePostal = value; }
        }

        /// <summary>
        /// Ville
        /// 
        /// Dans la db : VillePart
        /// </summary>
        private string ville;
        [DataMember]
        public string Ville
        {
            get { return ville; }
            set { ville = value; }
        }

        /// <summary>
        /// Pays
        ///
        /// /!\ Pas encore utilisé en date du : 28/01/2014 et pas non plus présent en base de donnée /!\
        /// /!\ Mais déjà prévu car possible que l'entreprise se développe à l'internationnal        /!\ 
        /// 
        /// Dans la db : /
        /// </summary>
        private string pays;
        [DataMember]
        public string Pays
        {
            get { return pays; }
            set { pays = value; }
        }

        /// <summary>
        /// Date de naissance
        /// 
        /// Dans la db : DateNaissPart
        /// </summary>
        private DateTime dateNaissance;
        [DataMember]
        public DateTime DateNaissance
        {
            get { return dateNaissance; }
            set { dateNaissance = value; }
        }

        /// <summary>
        /// Téléphone 1
        /// 
        /// Dans la db : TelPart1
        /// </summary>
        private string telephone1;
        [DataMember]
        public string Telephone1
        {
            get { return telephone1; }
            set { telephone1 = value; }
        }

        /// <summary>
        /// Téléphone 2
        /// 
        /// Dans la db : TelPart2
        /// </summary>
        private string telephone2;
        [DataMember]
        public string Telephone2
        {
            get { return telephone2; }
            set { telephone2 = value; }
        }

        /// <summary>
        /// Téléphone portable
        /// 
        /// Dans la db : TélPortPart
        /// </summary>
        private string telephonePortable;
        [DataMember]
        public string TelephonePortable
        {
            get { return telephonePortable; }
            set { telephonePortable = value; }
        }

        /// <summary>
        /// Email
        /// 
        /// Dans la db : EmailPart
        /// </summary>
        private string email;
        [DataMember]
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        /// <summary>
        /// Type de partenaire :
        ///
        /// 1 : Candidat Partenaire En Cours
        /// 2 : Partenaire Confirmé
        /// 3 : Candidat Partenaire Résilié
        /// 4 : Partenaire Coach
        /// 5 : Formation Cand Part
        /// 6 : Partenaire à archiver
        /// 7 : CNE en AFPE
        /// 8 : CNE confirmé
        /// 9 : CNE Résilié 
        /// 
        /// Dans la db : TypePart
        /// </summary>
        private int type;
        [DataMember]
        public int Type
        {
            get { return type; }
            set { type = value; }
        }

        /// <summary>
        /// Teléphone apparaissant sur la facture 
        /// 
        /// Dans la db : tel_part_fact
        /// </summary>
        private string telephoneFacture;
        [DataMember]
        public string TelephoneFacture
        {
            get { return telephoneFacture; }
            set { telephoneFacture = value; }
        }

        /// <summary>
        ///  1 : ancien barème an1=5 ans+=8 
        ///  2 : nouveau barème an1=,5 ans+=8   10% sans lettre 5% avec lettre sur la différence
        /// 
        /// Dans la db : bareme_com
        /// </summary>
        private int baremeCom;
        [DataMember]
        public int BaremeCom
        {
            get { return baremeCom; }
            set { baremeCom = value; }
        }
    }
}