﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

///
/// Auteur : Burléon Junior
/// Date de création : 03/02/2014
/// 
/// Attention à bien lire le commentaire de chaque attribut. Car un attribut ne porte pas toujours le même nom que celui qu'il porte en database.
/// La raison de cela est de donner des noms plus cohérant et plus parlant que ceux utilisé en database.
///

namespace BiocoldProcess.Domaine
{
    /// <summary>
    /// Classe définissant la journée d'un agent.
    /// </summary>
    /// [DataContract(Namespace = "http://extranetbcp.fr/WS/Domaine/JourneeAgent")]
    public class JourneeAgent
    {
        /// <summary>
        /// ID d'une journée
        /// 
        /// Dans la db : numauto_journag
        /// </summary>
        private int id;
        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// ID de l'agent ayant effectué la journée
        /// 
        /// Dans la db : init_agent
        /// </summary>
        private string idAgent;
        [DataMember]
        public string IdAgent
        {
            get { return idAgent; }
            set { idAgent = value; }
        }

        /// <summary>
        /// Date de la journée
        /// 
        /// Dans la db : datejourn
        /// </summary>
        private DateTime date;
        [DataMember]
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        /// <summary>
        /// Heure de départ le matin
        /// 
        /// Dans la db : h_dep_mat
        /// </summary>
        private DateTime heureDepartMatin;
        [DataMember]
        public DateTime HeureDepartMatin
        {
            get { return heureDepartMatin; }
            set { heureDepartMatin = value; }
        }

        /// <summary>
        /// Lieu de départ le matin
        /// 
        /// Dans la db : lieu_dep_mat
        /// </summary>
        private double lieuDepartMatin;
        [DataMember]
        public double LieuDepartMatin
        {
            get { return lieuDepartMatin; }
            set { lieuDepartMatin = value; }
        }

        /// <summary>
        /// Heure de la pause déjeune
        /// 
        /// Dans la db : h_stop_dej
        /// </summary>
        private DateTime heureStopDejeuner;
        [DataMember]
        public DateTime HeureStopDejeuner
        {
            get { return heureStopDejeuner; }
            set { heureStopDejeuner = value; }
        }

        /// <summary>
        /// Lieu de la pause déjeuner
        /// 
        /// Dans la db : lieu_stop_dej
        /// </summary>
        private double lieuStopDejeuner;
        [DataMember]
        public double LieuStopDejeuner
        {
            get { return lieuStopDejeuner; }
            set { lieuStopDejeuner = value; }
        }

        /// <summary>
        /// Heure de départ l'après-midi
        /// 
        /// Dans la db : h_dep_aprmid
        /// </summary>
        private DateTime heureDepartMidi;
        [DataMember]
        public DateTime HeureDepartMidi
        {
            get { return heureDepartMidi; }
            set { heureDepartMidi = value; }
        }

        /// <summary>
        /// Lieu de départ l'après-midi
        /// 
        /// Dans la db : lieu_dep_aprmid
        /// </summary>
        private double lieuDepartMidi;
        [DataMember]
        public double LieuDepartMidi
        {
            get { return lieuDepartMidi; }
            set { lieuDepartMidi = value; }
        }

        /// <summary>
        /// Heure de retour en fin de journée
        /// 
        /// Dans la db : h_retour
        /// </summary>
        private DateTime heureRetour;
        [DataMember]
        public DateTime HeureRetour
        {
            get { return heureRetour; }
            set { heureRetour = value; }
        }

        /// <summary>
        /// Lieu de retour en fin de journée
        /// 
        /// Dans la db : lieu_retour
        /// </summary>
        private double lieuRetour;
        [DataMember]
        public double LieuRetour
        {
            get { return lieuRetour; }
            set { lieuRetour = value; }
        }

        /// <summary>
        /// Observation du matin
        /// 
        /// Dans la db : obs_mat
        /// </summary>
        private string observationMatin;
        [DataMember]
        public string ObservationMatin
        {
            get { return observationMatin; }
            set { observationMatin = value; }
        }

        /// <summary>
        /// Observation de l'après-midi
        /// 
        /// Dans la db : obs_aprmid
        /// </summary>
        private string observationMidi;
        [DataMember]
        public string ObservationMidi
        {
            get { return observationMidi; }
            set { observationMidi = value; }
        }

        /// <summary>
        /// Total d'heure sur la journée
        /// 
        /// Dans la db : tot_h
        /// </summary>
        private float totalHeure;
        [DataMember]
        public float TotalHeure
        {
            get { return totalHeure; }
            set { totalHeure = value; }
        }

        /// <summary>
        /// Nombre d'heures au service de Biocold
        /// 
        /// Dans la db : h_service
        /// </summary>
        private float heureService;
        [DataMember]
        public float HeureService
        {
            get { return heureService; }
            set { heureService = value; }
        }

        /// <summary>
        /// Nombre de client à qui l'agent à rendu visite sur la journée
        /// 
        /// Dans la db : nb_clt_vis
        /// </summary>
        private int nombreClientVisite;
        [DataMember]
        public int NombreClientVisite
        {
            get { return nombreClientVisite; }
            set { nombreClientVisite = value; }
        }

        /// <summary>
        /// Nombre de client chez qui l'on a changé une ou plusieurs cassettes sur la journée
        /// 
        /// Dans la db : nb_clt_chg
        /// </summary>
        private int nombreClientChange;
        [DataMember]
        public int NombreClientChange
        {
            get { return nombreClientChange; }
            set { nombreClientChange = value; }
        }

        /// <summary>
        /// Nombre de client chez qui l'on a installé une ou plusieurs cassettes sur la journée
        /// 
        /// Dans la db : nb_clt_inst
        /// </summary>
        private int nombreClientInstalle;
        [DataMember]
        public int NombreClientInstalle
        {
            get { return nombreClientInstalle; }
            set { nombreClientInstalle = value; }
        }

        /// <summary>
        /// Nombre de client visité par heure
        /// 
        /// Dans la db : nb_clt_vis_par_h
        /// </summary>
        private int nombreClientVisiteParHeure;
        [DataMember]
        public int NombreClientVisiteParHeure
        {
            get { return nombreClientVisiteParHeure; }
            set { nombreClientVisiteParHeure = value; }
        }

        /// <summary>
        /// Nombre de grande cassette changé sur la journée
        /// 
        /// Dans la db : nb_gk7_chg
        /// </summary>
        private int nombreGrandeK7Change;
        [DataMember]
        public int NombreGrandeK7Change
        {
            get { return nombreGrandeK7Change; }
            set { nombreGrandeK7Change = value; }
        }

        /// <summary>
        /// Nombre de petite cassette changé sur la journée 
        /// 
        /// Dans la db : nb_pk7_chg
        /// </summary>
        private int nombrePetiteK7Change;
        [DataMember]
        public int NombrePetiteK7Change
        {
            get { return nombrePetiteK7Change; }
            set { nombrePetiteK7Change = value; }
        }

        /// <summary>
        /// Nombre de grande cassette installé sur la journée
        /// 
        /// Dans la db : nb_gk7_inst
        /// </summary>
        private int nombreGrandeK7Installe;
        [DataMember]
        public int NombreGrandeK7Installe
        {
            get { return nombreGrandeK7Installe; }
            set { nombreGrandeK7Installe = value; }
        }

        /// <summary>
        /// Nombre de petite cassette installé sur la journée 
        /// 
        /// Dans la db : nb_pk7_inst
        /// </summary>
        private int nombrePetiteK7Installe;
        [DataMember]
        public int NombrePetiteK7Installe
        {
            get { return nombrePetiteK7Installe; }
            set { nombrePetiteK7Installe = value; }
        }

        /// <summary>
        /// Activité effectué le matin 
        /// 
        /// Dans la db : acti_mat
        /// </summary>
        private int activiteMatin;
        [DataMember]
        public int ActiviteMatin
        {
            get { return activiteMatin; }
            set { activiteMatin = value; }
        }

        /// <summary>
        /// Activité effectué le matin
        /// 
        /// Dans la db : acti_apr
        /// </summary>
        private int activiteMidi;
        [DataMember]
        public int ActiviteMidi
        {
            get { return activiteMidi; }
            set { activiteMidi = value; }
        }

        /// <summary>
        /// Nombre de kilomètre parcouru le matin
        /// 
        /// Dans la db : km_matin
        /// </summary>
        private float kmMatin;
        [DataMember]
        public float KmMatin
        {
            get { return kmMatin; }
            set { kmMatin = value; }
        }

        /// <summary>
        /// Nombre de kilomètre parcouru l'après-midi
        /// 
        /// Dans la db : km_soir
        /// </summary>
        private float kmMidi;
        [DataMember]
        public float KmMidi
        {
            get { return kmMidi; }
            set { kmMidi = value; }
        }

        /// <summary>
        /// Nombre de kilomètre parcouru sur la journée
        /// 
        /// Dans la db : km_parc
        /// </summary>
        private float kmParcouru;
        [DataMember]
        public float KmParcouru
        {
            get { return kmParcouru; }
            set { kmParcouru = value; }
        }

        /// <summary>
        /// Nombre de refus sur la journée
        /// 
        /// Dans la db : refus
        /// </summary>
        private int nombreRefus;
        [DataMember]
        public int NombreRefus
        {
            get { return nombreRefus; }
            set { nombreRefus = value; }
        }
    }
}