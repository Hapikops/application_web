﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

///
/// Auteur : Burléon Junior
/// Date de création : 03/02/2014
/// 
/// Attention à bien lire le commentaire de chaque attribut. Car un attribut ne porte pas toujours le même nom que celui qu'il porte en database.
/// La raison de cela est de donner des noms plus cohérant et plus parlant que ceux utilisé en database.
///

namespace BiocoldProcess.Domaine
{
    /// <summary>
    /// Classe définissant un contrat.
    /// Attention : ici dans le projet il existe deux classe bien distincte Essai et Contrat mais en 
    /// base de donnée les deux se trouve persister dans la même table tblessaicontrat
    /// 
    /// Dans la database BCP : tblessaicontrat
    /// </summary>
    [DataContract(Namespace = "http://extranetbcp.fr/WS/Domaine/Contrat")]
    public class Contrat
    {
        /// <summary>
        /// ID du contrat. C'est en fait une concaténation de l'ID du partenaire ayant fait le contrat et du numéro de contrat
        /// 
        /// Dans la db : InitialesPartenaire + NumContrat
        /// </summary>
        private string id;
        [DataMember]
        public string Id
        {
            get { return id; }
            private set { id = value; }
        }

        /// <summary>
        /// ID de l'essai. Essai auquel est lié le contrat
        /// 
        /// Dans la db : N°Essai
        /// </summary>
        private int idEssai;
        [DataMember]
        public int IdEssai
        {
            get { return idEssai; }
            set { idEssai = value; }
        }

        /// <summary>
        /// ID du partenaire ayant fait le contrat.
        /// Si l'on modifie l'id du partenaire, l'id du contrat est mis à jour.
        /// 
        /// Dans la db : InitialesPartenaire
        /// </summary>
        private string idPartenaire;
        [DataMember]
        public string IdPartenaire
        {
            get { return idPartenaire; }
            set { idPartenaire = value; id = idPartenaire + numeroContrat.ToString(); }
        }

        /// <summary>
        /// Numéro de contrat.
        /// Il commence toujours à 751 et cela est bien vérifier lors du setter
        /// Si l'on modifie le numéro de contrat, l'id du contrat est mis à jour.
        /// 
        /// Dans la db : NumContrat
        /// </summary>
        private int numeroContrat;
        [DataMember]
        public int NumeroContrat
        {
            get { return numeroContrat; }
            set { if (value >= 751) numeroContrat = value; id = idPartenaire + numeroContrat.ToString(); }
        }

        /// <summary>
        /// Date à laquelle a eu lieu le denier passage chez le client
        /// 
        /// Dans la db : DateDernPass
        /// </summary>
        private DateTime dateDernierPassage;
        [DataMember]
        public DateTime DateDernierPassage
        {
            get { return dateDernierPassage; }
            set { dateDernierPassage = value; }
        }

        /// <summary>
        /// Date à laquelle a eu lieu le dernier changement de cassette
        /// 
        /// Dans la db : DateDernChgt
        /// </summary>
        private DateTime dateDernierChangement;
        [DataMember]
        public DateTime DateDernierChangement
        {
            get { return dateDernierChangement; }
            set { dateDernierChangement = value; }
        }

        /// <summary>
        /// ID du client permet de savoir à quel client est lié le contrat
        /// 
        /// Dans la db : N°Essai
        /// </summary>
        private int idClient;
        [DataMember]
        public int IdClient
        {
            get { return idClient; }
            set { idClient = value; }
        }

        /// <summary>
        /// Etat du contrat
        /// (Description des valeurs vient de la db)
        /// 1 : contrat en attente de validation par ASI
        /// 2 : contrat validé par ASI
        /// 3 : contrat non validé par ASI
        /// 4 : contrat en attente de résiliation par ASI
        /// 5 : contrat résilié par ASI
        /// 
        /// Dans la db : etatcont
        /// </summary>
        private int etat;
        [DataMember]
        public int Etat
        {
            get { return etat; }
            set { etat = value; }
        }
    }
}