﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

///
/// Auteur : Burléon Junior
/// Date de création : 30/01/2014
/// 
/// Attention à bien lire le commentaire de chaque attribut. Car un attribut ne porte pas toujours le même nom que celui qu'il porte en database.
/// La raison de cela est de donner des noms plus cohérant et plus parlant que ceux utilisé en database.
///

namespace BiocoldProcess.Domaine
{
    /// <summary>
    /// Classe définissant un agent.
    /// </summary>
    [DataContract(Namespace = "http://extranetbcp.fr/WS/Domaine/Agent")]
    public class Agent
    {
        /// <summary>
        /// ID de l'agent. C'est aussi ses initiales
        /// 
        /// Dans la db : InitialesAgent
        /// </summary>
        private string id;
        [DataMember]
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Nom de l'agent
        /// 
        /// Dans la db : NomAgent
        /// </summary>
        private string nom;
        [DataMember]
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        /// <summary>
        /// Prénom de l'agent
        /// 
        /// Dans la db : PrénomAgent
        /// </summary>
        private string prenom;
        [DataMember]
        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }

        /// <summary>
        /// Téléphone de l'agent
        /// 
        /// Dans la db : TélAgent
        /// </summary>
        private string telephone;
        [DataMember]
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        /// <summary>
        /// Téléphone portable de l'agent
        /// 
        /// Dans la db : TélPortAgent
        /// </summary>
        private string telephonePortable;
        [DataMember]
        public string TelephonePortable
        {
            get { return telephonePortable; }
            set { telephonePortable = value; }
        }

        /// <summary>
        /// Id du partenaire auquel est lié l'agent
        /// 
        /// Dans la db : InitPart
        /// </summary>
        private string idPartenaire;
        [DataMember]
        public string IdPartenaire
        {
            get { return idPartenaire; }
            set { idPartenaire = value; }
        }

        /// <summary>
        /// Date entrée chez le partenaire
        /// 
        /// Dans la db : DateEntreeAgent
        /// </summary>
        private DateTime dateEntree;
        [DataMember]
        public DateTime DateEntree
        {
            get { return dateEntree; }
            set { dateEntree = value; }
        }

        /// <summary>
        /// Date de sortie de chez le partenaire
        /// 
        /// Dans la db : DateSortieAgent
        /// </summary>
        private DateTime dateSortie;
        [DataMember]
        public DateTime DateSortie
        {
            get { return dateSortie; }
            set { dateSortie = value; }
        }

        /// <summary>
        /// Indique si l'Agent travaille effectivement chez le Partenaires et fait partie de la liste 
        /// dynamique des Agents dans le formulaire saisieservice.asp
        /// 
        /// Dans la db : AgentActif
        /// </summary>
        private bool actif;
        [DataMember]
        public bool Actif
        {
            get { return actif; }
            set { actif = value; }
        }

        /// <summary>
        /// ID de l'utilisateur auquel est lié l'agent.
        /// 
        /// Dans la db : id_util
        /// </summary>
        private int idUtilisateur;
        [DataMember]
        public int IdUtilisateur
        {
            get { return idUtilisateur; }
            set { idUtilisateur = value; }
        }

    }
}