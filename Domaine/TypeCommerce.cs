﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

///
/// Auteur : Burléon Junior
/// Date de création : 03/04/2014
/// 
/// Attention à bien lire le commentaire de chaque attribut. Car un attribut ne porte pas toujours le même nom que celui qu'il porte en database.
/// La raison de cela est de donner des noms plus cohérant et plus parlant que ceux utilisé en database.
///

namespace BiocoldProcess.Domaine
{
    /// <summary>
    /// Classe définissant un Type de commerce.
    /// 
    /// Dans la database BCP : tbl_TYPE_COMMERCE
    /// </summary>
    [DataContract]
    public class TypeCommerce
    {
        /// <summary>
        /// Id de TypeCommerce : 
        /// 
        /// le champs: numauto_typcom
        /// </summary>
        private int id;
        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// value de TypeCommerce est la valeur réelle que l'on peut afficher, du type de commerce
        /// 
        /// le champs: TYPE_COMMERCE
        /// </summary>
        private string value;
        [DataMember]
        public string Value
        {
            get { return value; }
            set { this.value = value; }
        }
    }
}