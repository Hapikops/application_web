﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

///
/// Auteur : Burléon Junior
/// Date de création : 03/02/2014
/// 
/// Attention à bien lire le commentaire de chaque attribut. Car un attribut ne porte pas toujours le même nom que celui qu'il porte en database.
/// La raison de cela est de donner des noms plus cohérant et plus parlant que ceux utilisé en database.
///

namespace BiocoldProcess.Domaine
{
    /// <summary>
    /// Classe définissant le passage d'un agent chez un client.
    /// </summary>
    [DataContract(Namespace = "http://extranetbcp.fr/WS/Domaine/PassageAgentWS")]
    public class PassageAgent
    {
        /// <summary>
        /// ID du passage.
        /// Attention dans la database, dans la table tblPassageAgent, le nom utilisé est : NumChgt
        /// 
        /// Dans la db : NumChgt
        /// </summary>
        private int id;
        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Le nombre de passage effectué au total
        /// 
        /// Dans la db : N°ChgtPart
        /// </summary>
        private int nombrePassage;

        public int NombrePassage
        {
            get { return nombrePassage; }
            set { nombrePassage = value; }
        }

        /// <summary>
        /// ID de l'essai auquel est lié le passage
        /// 
        /// Dans la db : N°Essai
        /// </summary>
        private int idEssai;
        [DataMember]
        public int IdEssai
        {
            get { return idEssai; }
            set { idEssai = value; }
        }

        /// <summary>
        /// ID de l'essai auquel est lié le passage
        /// 
        /// Dans la db : NumEssaiPart
        /// </summary>
        private int idEssaiPartenaire;
        [DataMember]
        public int IdEssaiPartenaire
        {
            get { return idEssaiPartenaire; }
            set { idEssaiPartenaire = value; }
        }

        /// <summary>
        /// ID du contrat auquel est lié le passage si un contrat existe
        /// Si il n'y pas de contrat alors ce champs vaut null
        /// 
        /// </summary>
        private string idContrat;
        [DataMember]
        public string IdContrat
        {
            get { return idContrat; }
            set { idContrat = value; }
        }

        /// <summary>
        /// ID de l'agent effectuant le passage
        /// 
        /// Dans la db : InitialesAgent
        /// </summary>
        private string idAgent;
        [DataMember]
        public string IdAgent
        {
            get { return idAgent; }
            set { idAgent = value; }
        }

        /// <summary>
        /// Commentaire copié du champ dans MS Access
        /// Attention ce champ contient le code du type de passage de la table tbl_type_bis_part
        /// 
        /// Dans la db : TypePassage
        /// </summary>
        private string typePassage;
        [DataMember]
        public string TypePassage
        {
            get { return typePassage; }
            set { typePassage = value; }
        }

        /// <summary>
        /// DateSaisie de passage (supposé chez le client)
        /// D'après un commentaire sur le champ dans MS Access, celui-ci ne serait plus utilisé, pourtant les dernier record le remplisse bien.
        /// Je décide donc qu'il vaut mieux prévoir le champ et peut-être le supprimer plus tard.
        /// 
        /// Dans la db : DatePassage
        /// </summary>
        private DateTime datePassage;
        [DataMember]
        public DateTime DatePassage
        {
            get { return datePassage; }
            set { datePassage = value; }
        }

        /// <summary>
        /// Heure à la quel est effectué le passage chez le client.
        /// 
        /// Dans la db : heurePass
        /// </summary>
        private DateTime heurePassage;
        [DataMember]
        public DateTime HeurePassage
        {
            get { return heurePassage; }
            set { heurePassage = value; }
        }

        /// <summary>
        /// Le temps qu'a duré un passage en minute.
        /// 
        /// Dans la db : duree_pass
        /// </summary>
        private int dureePassage;
        [DataMember]
        public int DureePassage
        {
            get { return dureePassage; }
            set { dureePassage = value; }
        }

        /// <summary>
        /// Cette attribut devrait contenir la date de changement d'une cassette mais d'après le commentaire
        /// sur le champ dans MS Access, ce champ contiendrait maintenant la date de passage.
        /// Si l'on part du principe que l'on fait un passage que pour faire un changement, cela ne doit pas posé de problème
        /// si se n'est qu'il y ait une répétition d'information.
        /// 
        /// Dans la db : DateChgtK7
        /// </summary>
        private DateTime dateChangementK7;
        [DataMember]
        public DateTime DateChangementK7
        {
            get { return dateChangementK7; }
            set { dateChangementK7 = value; }
        }

        /// <summary>
        /// DateSaisie de saisie de l'enregistrement
        /// 
        /// Dans la db : DateSaisieChgt
        /// </summary>
        private DateTime dateSaisieChangement;
        [DataMember]
        public DateTime DateSaisieChangement
        {
            get { return dateSaisieChangement; }
            set { dateSaisieChangement = value; }
        }

        /// <summary>
        /// Observations internes saisies par le partenaire.
        /// Informations non communiquées au client.
        /// 
        /// Dans la db : ObsPassInt
        /// </summary>
        private string observationPassageInterne;
        [DataMember]
        public string ObservationPassageInterne
        {
            get { return observationPassageInterne; }
            set { observationPassageInterne = value; }
        }

        /// <summary>
        /// Observations à transmettre au client lors du prochain changement
        /// 
        /// Dans la db : ObsPassClt
        /// </summary>
        private string observationPassageClient;
        [DataMember]
        public string ObservationPassageClient
        {
            get { return observationPassageClient; }
            set { observationPassageClient = value; }
        }

        /// <summary>
        /// ID de l'utilisateur qui effectue l'enregistrement
        /// 
        /// Dans la db : IdUtilSaisie
        /// </summary>
        private int idUtilisateur;
        [DataMember]
        public int IdUtilisateur
        {
            get { return idUtilisateur; }
            set { idUtilisateur = value; }
        }

        /// <summary>
        /// Visite de Résolution du Contrat ? Oui / Non
        /// 
        /// Dans la db : vis_vrc
        /// </summary>
        private bool visiteResolutionContrat;
        [DataMember]
        public bool VisiteResolutionContrat
        {
            get { return visiteResolutionContrat; }
            set { visiteResolutionContrat = value; }
        }

        /// <summary>
        /// 1 : Refus confirmé
        /// 2 : Contrat sauvé
        /// 
        /// Dans la db : result_vrc
        /// </summary>
        private int resultatResolutionContrat;
        [DataMember]
        public int ResultatResolutionContrat
        {
            get { return resultatResolutionContrat; }
            set { resultatResolutionContrat = value; }
        }

        /// <summary>
        /// Observations lors de la mise en attente de résiliation
        /// 
        /// Dans la db : obs_refus
        /// </summary>
        private string observationRefus = "";
        [DataMember]
        public string ObservationRefus
        {
            get { return observationRefus; }
            set { observationRefus = value; }
        }
    }
}