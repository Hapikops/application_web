﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

///
/// Auteur: Frederic Radigoy
/// Date de creation : 17/03/2014
/// 
/// Attention à bien lire le commentaire de chaque attribut. Car un attribut ne porte pas toujours le même nom que celui qu'il porte en database.
/// La raison de cela est de donner des noms plus cohérant et plus parlant que ceux utilisé en database.
///

namespace BiocoldProcess.Domaine
{
    /// <summary>
    /// Cette classe definit un type de Passage chez un client
    /// 
    /// Dans la database BCP : tbl_type_vis_part
    /// </summary>
    [DataContract]
    public class TypePassage
    {
        /// <summary>
        /// Id de TypePassage : 
        /// 
        /// le champs: numauto_typ_vis
        /// </summary>
        private int id;
        [DataMember]
        public int Id 
        {
            get { return id; }
            set { id = value; } 
        }

        /// <summary>
        /// Key de TypePassage : abréviation du typeCommerce
        /// 
        /// le champs: lettre_type_vis
        /// </summary>
        private string key;
        [DataMember]
        public string Key 
        {
            get { return key; }
            set { key = value; }
        }

        /// <summary>
        /// value de TypePassage  est la description du type de passage chez un client
        /// 
        /// le champs: type_vis
        /// </summary>
        private string value;
        [DataMember]
        public string Value 
        {
            get { return value; }
            set { this.value = value; }
        }

        /// <summary>
        /// Categorie représente le fait que le passage peut-être lié à 
        /// - Un essai : 1
        /// - Un contrat : 2
        /// - Un Contrat ou Un essai : 3
        /// 
        /// le champs: Categorie
        /// </summary>
        private int categorie;
        [DataMember]
        public int Categorie
        {
            get { return categorie; }
            set { this.categorie = value; }
        }
    }
}