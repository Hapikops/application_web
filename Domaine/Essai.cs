﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

///
/// Auteur : Burléon Junior
/// Date de création : 03/02/2014
/// 
/// Attention à bien lire le commentaire de chaque attribut. Car un attribut ne porte pas toujours le même nom que celui qu'il porte en database.
/// La raison de cela est de donner des noms plus cohérant et plus parlant que ceux utilisé en database.
///

namespace BiocoldProcess.Domaine
{
    /// <summary>
    /// Classe définissant un essai.
    /// </summary>
    public class Essai
    {
        /// <summary>
        /// ID de l'essai.
        /// 
        /// Dans la db : N°Essai
        /// </summary>
        private int id;
        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// ID de l'essai pour le partenaire.
        /// Cette ID est présent pour que le partenaire ne puisse pas savoir le nombre d'essai réelle qui sont mené.
        /// 
        /// Dans la db : NumEssaiPart
        /// </summary>
        private int idEssaiPartenaire;
        [DataMember]
        public int IdEssaiPartenaire
        {
            get { return idEssaiPartenaire; }
            set { idEssaiPartenaire = value; }
        }

        /// <summary>
        /// ID du partenaire ayant fait l'essai.
        /// 
        /// Dans la db : InitialesPartenaire
        /// </summary>
        private string idPartenaire;
        [DataMember]
        public string IdPartenaire
        {
            get { return idPartenaire; }
            set { idPartenaire = value; }
        }

        /// <summary>
        /// Date à laquelle a eu lieu le denier passage chez le client
        /// 
        /// Dans la db : DateDernPass
        /// </summary>
        private DateTime dateDernierPassage;
        [DataMember]
        public DateTime DateDernierPassage
        {
            get { return dateDernierPassage; }
            set { dateDernierPassage = value; }
        }

        /// <summary>
        /// Date à laquelle a eu lieu le dernier changement de cassette
        /// 
        /// Dans la db : DateDernChgt
        /// </summary>
        private DateTime dateDernierChangement;
        [DataMember]
        public DateTime DateDernierChangement
        {
            get { return dateDernierChangement; }
            set { dateDernierChangement = value; }
        }

        /// <summary>
        /// Date du début de l'essai
        /// 
        /// Dans la db : DateDebutEssai
        /// </summary>
        private DateTime dateDebut;
        [DataMember]
        public DateTime DateDebut
        {
            get { return dateDebut; }
            set { dateDebut = value; }
        }

        /// <summary>
        /// ID du client permet de savoir à quel client est lié le contrat
        /// 
        /// Dans la db : N°Essai
        /// </summary>
        private int idClient;
        [DataMember]
        public int IdClient
        {
            get { return idClient; }
            set { idClient = value; }
        }

        /// <summary>
        /// Permet de savoir si un essai est terminé
        /// 
        /// Dans la db : Ce champ est calculé grâce aux champ suivant.
        /// Champ : K7retirees1erRdvSign
        /// Champ : K7retirees2emeRdvSign
        /// Champ : Result3emeRdv
        /// Champ : NumContrat
        /// 
        /// if(K7retirees1erRdvSign == true OR K7retirees2emeRdvSign == true OR Result3emeRdv == 2 OR NumContrat != null)
        ///     Completed = TRUE
        /// else 
        ///     Completed = FALSE
        ///     
        /// </summary>
        private bool completed;
        [DataMember]
        public bool Completed
        {
            get { return completed; }
            set { completed = value; }
        }
    }
}