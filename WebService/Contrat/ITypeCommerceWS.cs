﻿using BiocoldProcess.Domaine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

///
/// Auteur : Burléon Junior
/// Date de création : 03/04/2014
/// 

namespace BiocoldProcess.WebService.ContratWS
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "ITypeCommerceWS" à la fois dans le code et le fichier de configuration.
    [ServiceContract(Namespace = "http://extranetbcp.fr/WebService/Implementation/TypeCommerceWS.svc")]
    public interface ITypeCommerceWS
    {
        /// <summary>
        /// Cette méthode permet de récupérer la liste complète des type des commerce
        /// </summary>
        /// <returns>Retourne la liste des types de commerce.</returns>
        [OperationContract]
        [WebGet(UriTemplate = "list", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        List<TypeCommerce> getList();
    }
}
