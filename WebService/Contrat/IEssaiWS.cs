﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 21/03/2014
/// 

namespace BiocoldProcess.WebService.ContratWS
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IEssaiWS" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    public interface IEssaiWS
    {
        /// <summary>
        /// Cette méthode permet de récupérer uniquement les ID des essai d'un partenaire en particulier.
        /// </summary>
        /// <param name="idPartenaire">Identifiant d'un partenaire.</param>
        /// <returns>Retourne tout les essais lié au partenaire.</returns>
        [OperationContract]
        [WebGet(UriTemplate = "listWithoutContract/{idPartenaire}", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        List<Essai> getListEssai(string idPartenaire);

        /// <summary>
        /// Cette méthode permet de récupérer toutes les informations sur un essai.
        /// </summary>
        /// <param name="idEssai">Identifiant permettant de retrouver un essai en db.</param>
        /// <returns>Retourne toute les informations sur un essai</returns>
        [OperationContract]
        [WebGet(UriTemplate = "{idEssai}", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        Essai getEssai(string idEssai);

        /// <summary>
        /// Permet au webservice d'insérer des essais en base de donnée
        /// </summary>
        /// <param name="essai">L'objet essai à ajouter en base de donnée</param>
        /// <returns>Retourne l'id de l'essai inséré. Retourne aussi les code HTTP suivant 200 ou 400 en fonction de la situation. Si problème la valeur de retour vaut -1 </returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "", RequestFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        int addEssai(Essai essai);
    }
}
