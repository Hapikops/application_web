﻿using BiocoldProcess.Domaine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

///
/// Auteur : Burléon Junior
/// Date de création : 11/02/2014
/// 

namespace BiocoldProcess.WebService.ContratWS
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IContratWS" à la fois dans le code et le fichier de configuration.
    [ServiceContract(Namespace = "http://extranetbcp.fr/WS/Service/ContratWS")]
    public interface IContratWS
    {
        /// <summary>
        /// Cette méthode permet de récupérer toutes les informations sur un contrat.
        /// </summary>
        /// <param name="idContrat">Identifiant permettant de retrouver un contrat en db.</param>
        /// <returns>Retourne toute les informations sur un contrat</returns>
        [OperationContract]
        [WebGet(UriTemplate = "{idContrat}", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        Contrat getContrat(string idContrat);


        /// <summary>
        /// Cette méthode permet de récupérer uniquement les ID des contrat d'un partenaire en particulier.
        /// </summary>
        /// <param name="idPartenaire">Identifiant d'un partenaire.</param>
        /// <returns>Retourne tout les contrats (ID uniquement) lié au partenaire.</returns>
        [OperationContract]
        [WebGet(UriTemplate = "list/{idPartenaire}", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        List<Contrat> getListContrat(string idPartenaire);
    }
}
