﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 04/02/2014
/// 
/// Auteur de Modification: Radigoy frederic
/// Date de Modification: 17/03/2014
/// 

namespace BiocoldProcess.WebService.ContratWS
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IPassageAgentWS" à la fois dans le code et le fichier de configuration.
    [ServiceContract(Namespace = "http://extranetbcp.fr/WebService/Implementation/PassageAgentWS.svc")]
    public interface IPassageAgentWS
    {
        /// <summary>
        /// Permet au webservice d'insérer des passage d'agent en base de donnée
        /// </summary>
        /// <param name="contractDataPassageAgent">Ce paramètre est un objet qui contient toute les information pour une insertion correcte</param>
        /// <returns>Retourne l'id du passage inséré. Retourne aussi les code HTTP suivant 200, 400 et 404 en fonction de la situation</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "",RequestFormat = WebMessageFormat.Xml,BodyStyle = WebMessageBodyStyle.Bare)]
        int faireUnPassage(ContractDataPassageAgent contractDataPassageAgent);

        /// <summary>
        /// Permet de récupérer la liste des passage de la dernière semaine écoulée en fonction d'un agent
        /// </summary>
        /// <param name="idAgent">ID de l'agent par rapport auquel sont sélectionné les passages</param>
        /// <returns>Retourne la liste de passage</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "list/{idAgent}", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        List<PassageAgent> list(string idAgent);

        /// <summary>
        /// Permet de récupérer la liste de type de passage
        /// </summary>
        /// <returns>Retourne la la liste de type de passage</returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "listTypePassage", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        List<TypePassage> listTypePassage();
    }

    /// <summary>
    /// Contrat de donnée permettant de récupérer un scan de cassette
    /// 
    /// Cette classe est utilisé par la classe ContractDataPassageAgent
    /// </summary>
    [DataContract(Namespace = "http://extranetbcp.fr/WS/Service/ContractDataScanK7")]
    public class ContractDataScanK7
    {
        /// <summary>
        /// ID de l'utilisateur ayant effectué le scan du code barre
        /// </summary>
        private string idUser;
        [DataMember(Order = 1)]
        public string IdUser { get { return idUser; } set { idUser = value; } }

        /// <summary>
        /// Le code barre en lui même
        /// </summary>
        string codeBarre;
        [DataMember(Order = 2)]
        public string CodeBarre { get { return codeBarre; } set { codeBarre = value; } }

        /// <summary>
        /// La date et l'heure à laquelle a été effectué le scan
        /// </summary>
        string date;
        [DataMember(Order = 3)]
        public string Date { get { return date; } set { date = value; } }

        /// <summary>
        /// La position lors du scan (ici la longitude)
        /// </summary>
        string longitude;
        [DataMember(Order = 4)]
        public string Longitude { get { return longitude; } set { longitude = value; } }

        /// <summary>
        /// La position lors du scan (ici la latitude)
        /// </summary>
        string latitude;
        [DataMember(Order = 5)]
        public string Latitude { get { return latitude; } set { latitude = value; } }

        /// <summary>
        /// ID de l'essai/contrat auquel est lié la cassette scannée
        /// </summary>
        string idEssai;
        [DataMember(Order = 6)]
        public string IdEssai { get { return idEssai; } set { idEssai = value; } }

        /// <summary>
        /// Etat de la cassette.
        /// Cette attribut correspond à l'id d'un état dans la table : tblListeEtatK7 qui est dans la db BCP_LP
        /// </summary>
        private string etatK7;
        [DataMember(Order = 7)]
        public string EtatK7
        {
            get { return etatK7; }
            set { etatK7 = value; }
        }
    }



    /// <summary>
    /// Contrat de donnée permettant de récupéré un passage d'agent
    /// </summary>
    [DataContract(Namespace = "http://extranetbcp.fr/WS/Service/contractDataPassageAgent")]
    public class ContractDataPassageAgent
    {
        /// <summary>
        /// ID de l'utilisateur ayant effectué le scan du code barre
        /// </summary>
        private string idUser;
        [DataMember(Order = 1)]
        public string IdUser { get { return idUser; } set { idUser = value; } }

        /// <summary>
        /// La date et l'heure à laquelle est effectué le pasage
        /// </summary>
        string date;
        [DataMember(Order = 2)]
        public string Date { get { return date; } set { date = value; } }

        /// <summary>
        /// ID de l'agent effectuant le pasage
        /// </summary>
        string idAgent;
        [DataMember(Order = 3)]
        public string IdAgent { get { return idAgent; } set { idAgent = value; } }

        /// <summary>
        /// ID de l'essai/contrat auquel est lié le passage
        /// </summary>
        string idEssai;
        [DataMember(Order = 4)]
        public string IdEssai { get { return idEssai; } set { idEssai = value; } }

        /// <summary>
        /// Observation interne que l'agent peut faire lors de son passage
        /// </summary>
        string observationPassageInterne;
        [DataMember(Order = 5)]
        public string ObservationPassageInterne { get { return observationPassageInterne; } set { observationPassageInterne = value; } }

        private string typePassage;
        [DataMember(Order = 6)]
        public string TypePassage { get { return typePassage; } set { typePassage = value; } }

        /// <summary>
        /// Liste des scans lié au passage
        /// </summary>
        private List<ContractDataScanK7> listContractDataScanK7;

        [DataMember(Order = 7)]
        public List<ContractDataScanK7> ListContractDataScanK7 { get { return listContractDataScanK7; } set { listContractDataScanK7 = value; } }
    }



}
