﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 12/02/2014
/// 

namespace BiocoldProcess.WebService.ContratWS
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IClientWS" à la fois dans le code et le fichier de configuration.
    [ServiceContract(Namespace = "http://extranetbcp.fr/WS/Service/ClientWS")]
    public interface IClientWS
    {
        /// <summary>
        /// Cette méthode permet de récupérer toutes les informations sur un client.
        /// </summary>
        /// <param name="idClient">Identifiant permettant de retrouver un client en db.</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{idClient}", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        Client getClient(string idClient);

        /// <summary>
        /// Permet au webservice d'editer (mettre à jour) un client en base de donnée
        /// </summary>
        /// <param name="client">L'objet client à mettre à jour en base de donnée</param>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "", RequestFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        void editClient(Client client);

        /// <summary>
        /// Permet au webservice d'ajouter un client en base de donnée
        /// </summary>
        /// <param name="client">L'objet client à ajouter en base de donnée</param>
        /// /// <returns>Retourne l'id du passage inséré. Retourne aussi les code HTTP suivant 200 ou 400 en fonction de la situation. id retourné -1 si erreur</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "", RequestFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        int addClient(Client client);
    }
}
