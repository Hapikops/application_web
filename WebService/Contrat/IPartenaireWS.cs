﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 30/01/2014
/// 

namespace BiocoldProcess.WebService.ContratWS
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le login d'interface "IService1" à la fois dans le code et le fichier de configuration.
    [ServiceContract (Namespace="http://extranetbcp.fr/WS/Service/PartenaireWS")]
    public interface IPartenaireWS
    {
        /// <summary>
        /// Cette méthode permet de récupérer toutes les informations sur un partenaire.
        /// </summary>
        /// <param name="InitialPartenaire">Identifiant permettant de retrouver un partenaire en db.</param>
        /// <returns>Retourne toute les informations sur un partenaire</returns>
        [OperationContract]
        [WebGet(UriTemplate = "{initialPartenaire}", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        Partenaire getPartenaire(string initialPartenaire);
    }
}

