﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 12/02/2014
/// 

namespace BiocoldProcess.WebService.ContratWS
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IAgentWS" à la fois dans le code et le fichier de configuration.
    [ServiceContract(Namespace = "http://extranetbcp.fr/WS/Service/AgentWS")]
    public interface IAgentWS
    {
        /// <summary>
        /// Cette méthode permet de récupérer les informations d'un agent précis en fonction de l'id utilisateur qui est envoyer.
        /// Par exemple pour l'application mobile il est utile de pouvoir récupérer les informations d'un agent qui vient de se connecter.
        /// </summary>
        /// <param name="idUser">C'est grâce à cette id que l'on va récupéré un agent précis en db</param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "{idUser}", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        Agent getAgent(string idUser);
    }
}
