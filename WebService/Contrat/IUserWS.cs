﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 03/02/2014
/// 

namespace BiocoldProcess.WebService.ContratWS
{
    [ServiceContract(Namespace = "http://extranetbcp.fr/WS/Service/UserWS")]
    public interface IUserWS
    {
        /// <summary>
        /// Cette méthode doit permettre de s'authentifier à distance
        /// </summary>
        /// <param name="login">Nom avec lequel on doit se connecter</param>
        /// <param name="password">Mot de passe correspondant au login</param>
        /// <returns>
        /// Si l'authentification réussi ça retourne toute les informations sur l'utilisateur et HTTP OK
        /// Si ça échoue : 
        ///     - Login inexistant : Erreur HTTP NotFound
        ///     - Mot de passe inccorecte : Erreur HTTP NotAcceptable
        ///     Et le retour de utilisateur sera null.
        /// </returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate="{login}", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Bare)]
        Utilisateur login(string login);
    }
}
