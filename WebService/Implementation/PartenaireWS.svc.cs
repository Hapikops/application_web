﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using BiocoldProcess.Domaine;
using WSBiocold.IDao;
using BiocoldProcess.WebService.ContratWS;
using BiocoldProcess.Dao;

///
/// Auteur : Burléon Junior
/// Date de création : 30/01/2014
/// 

namespace BiocoldProcess.WebService.Implementation
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le login de classe "PartenaireWS" dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez PartenaireWS.svc ou PartenaireWS.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public class PartenaireWS : IPartenaireWS
    {
        private IPartenaireDao partenaireDao = new PartenaireDao();

        public Partenaire getPartenaire(string initialPartenaire)
        {
            

            Partenaire partenaire = partenaireDao.find(initialPartenaire);

            if (partenaire == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                return partenaire;
            }
        }
    }
}
