﻿using BiocoldProcess.Dao;
using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;
using BiocoldProcess.Tools;
using BiocoldProcess.WebService.ContratWS;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

///
/// Auteur : Burléon Junior
/// Date de création : 04/02/2014
/// 
/// Auteur de Modification: Radigoy frederic
/// Date de modification : 17/03/2014
/// 

namespace BiocoldProcess.WebService.Implementation
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "PassageAgentWS" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez PassageAgentWS.svc ou PassageAgentWS.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class PassageAgentWS : IPassageAgentWS
    {
        private IPassageAgentDao passageAgentDao = new PassageAgentDao();
        private IScanK7Dao scanK7Dao = new ScanK7Dao();
        private IContratDao contratDao = new ContratDao();
        private IEssaiDao essaiDao = new EssaiDao();
        private ITypePassageDao typePassageDao = new TypePassageDao();



        public int faireUnPassage(ContractDataPassageAgent contractDataPassageAgent)
        {
            int id = -1;
            try
            {
                PassageAgent passageAgent = new PassageAgent();
                passageAgent.NombrePassage = 0; //Pas encore gérer
                passageAgent.IdEssai = int.Parse(contractDataPassageAgent.IdEssai);
                passageAgent.IdAgent = contractDataPassageAgent.IdAgent;

               
                passageAgent.TypePassage = contractDataPassageAgent.TypePassage; 
                passageAgent.DatePassage = Utility.JavaTimeStampToDateTime(long.Parse(contractDataPassageAgent.Date));
                passageAgent.HeurePassage = Utility.JavaTimeStampToDateTime(long.Parse(contractDataPassageAgent.Date));
                passageAgent.DureePassage = 0; //Pas encore gérer
                passageAgent.DateChangementK7 = Utility.JavaTimeStampToDateTime(long.Parse(contractDataPassageAgent.Date));
                passageAgent.DateSaisieChangement = Utility.JavaTimeStampToDateTime(long.Parse(contractDataPassageAgent.Date));
                if (contractDataPassageAgent.ObservationPassageInterne != "")
                    passageAgent.ObservationPassageInterne = contractDataPassageAgent.ObservationPassageInterne;
                else
                    passageAgent.ObservationPassageInterne = " "; //Car ce champ ne peut être réellement vide en DB
                passageAgent.ObservationPassageInterne = " ";
                passageAgent.ObservationPassageClient = ""; //Pas encore gérer
                passageAgent.IdUtilisateur = int.Parse(contractDataPassageAgent.IdUser);

                //Ajout à la db du passage et récupération de l'id de celui-ci
                id = (int)passageAgentDao.add(passageAgent);

                //Ajoute tout les scans de cassette à la db en les liant au passage
                foreach (ContractDataScanK7 contractDataScanK7 in contractDataPassageAgent.ListContractDataScanK7)
                {
                    //Initialisation de l'objet scanK7
                    ScanK7 scanK7 = new ScanK7();
                    scanK7.Code = contractDataScanK7.CodeBarre;
                    scanK7.IdUser = int.Parse(contractDataScanK7.IdUser);
                    scanK7.DateSaisie = Utility.JavaTimeStampToDateTime(long.Parse(contractDataScanK7.Date));
                    if (contractDataScanK7.Longitude != "null")
                        scanK7.Longitude = double.Parse(contractDataScanK7.Longitude, CultureInfo.InvariantCulture);
                    if (contractDataScanK7.Longitude != "null")
                        scanK7.Latitude = double.Parse(contractDataScanK7.Latitude, CultureInfo.InvariantCulture);
                    scanK7.EtatDate = Utility.JavaTimeStampToDateTime(long.Parse(contractDataScanK7.Date));
                    scanK7.EtatK7 = int.Parse(contractDataScanK7.EtatK7);
                    scanK7.IdEssai = int.Parse(contractDataScanK7.IdEssai);
                    scanK7.IdPassageAgent = id;

                    //Envoie en db
                    scanK7Dao.add(scanK7);
                }

                Contrat contrat = new Contrat();
                contrat = contratDao.find(passageAgent.IdEssai);
                //Dans le cas où le passage est lié à un essai
                if(contrat.NumeroContrat == null)
                {
                    Essai essai = new Essai();
                    essai = essaiDao.find(passageAgent.IdEssai);
                    essai.DateDernierPassage = passageAgent.DatePassage;
                    essai.DateDernierChangement = passageAgent.DateChangementK7;
                    essaiDao.update(essai);

                }
                //Dans le cas où le passage est lié à un contrat 
                else
                {
                    //Mise à jour de la date de dernier passage et dernier changement pour le contrat.
                    contrat.DateDernierPassage = passageAgent.DatePassage;
                    contrat.DateDernierChangement = passageAgent.DateChangementK7;
                    contratDao.update(contrat);
                }

                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
            }
            catch
            {
                try
                {
                    WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                }
                catch
                { }
            }

            return id;
        }

        public List<PassageAgent> list(string idAgent)
        {
            List<PassageAgent> listPassage = passageAgentDao.list(idAgent);

            if (listPassage == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                return null;
            }
            if (listPassage.Count == 0)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                return listPassage;
            }
        }


        public List<TypePassage> listTypePassage()
        {
            List<TypePassage> listTypePassage = typePassageDao.list();

            if (listTypePassage == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                return null;
            }
            if (listTypePassage.Count == 0)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                return listTypePassage;
            }
        }
    }
}
