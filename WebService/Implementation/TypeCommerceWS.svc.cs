﻿using BiocoldProcess.Dao;
using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;
using BiocoldProcess.WebService.ContratWS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace BiocoldProcess.WebService.Implementation
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "TypeCommerceWS" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez TypeCommerce.svc ou TypeCommerce.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class TypeCommerceWS : ITypeCommerceWS
    {
        private ITypeCommerceDao typeCommerceDao = new TypeCommerceDao();

        public List<TypeCommerce> getList()
        {
            List<TypeCommerce> listTypeCommerce = typeCommerceDao.list();

            if (listTypeCommerce == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                return null;
            }
            if (listTypeCommerce.Count == 0)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                return listTypeCommerce;
            }
        }
    }
}
