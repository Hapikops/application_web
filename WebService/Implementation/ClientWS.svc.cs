﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;
using BiocoldProcess.WebService.ContratWS;
using BiocoldProcess.Dao;

///
/// Auteur : Burléon Junior
/// Date de création : 12/02/2014
/// 

namespace BiocoldProcess.WebService.Implementation
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "ClientWS" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez ClientWS.svc ou ClientWS.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class ClientWS : IClientWS
    {
        private IClientDao clientDao = new ClientDao();
        public Client getClient(string idClient)
        {
            Client client = clientDao.find(int.Parse(idClient));

            if (client == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                return client;
            }
        }

        public void editClient(Client client)
        {
            try
            {
                clientDao.update(client);
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
            }
            catch
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }
        }


        public int addClient(Client client)
        {
            int id = -1;

            try
            {
                id = (int)clientDao.add(client);
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
            }
            catch
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return id;
        }
    }
}
