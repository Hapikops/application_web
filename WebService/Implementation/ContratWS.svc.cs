﻿using BiocoldProcess.Dao;
using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;
using BiocoldProcess.WebService.ContratWS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

///
/// Auteur : Burléon Junior
/// Date de création : 11/02/2014
/// 

namespace BiocoldProcess.WebService.Implementation
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "ContratWS" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez ContratWS.svc ou ContratWS.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public class ContratWS : IContratWS
    {
        private IContratDao contratDao = new ContratDao();

        public BiocoldProcess.Domaine.Contrat getContrat(string idContrat)
        {
            BiocoldProcess.Domaine.Contrat contrat = contratDao.find(idContrat);

            if (contrat == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                return contrat;
            }
        }


        public List<BiocoldProcess.Domaine.Contrat> getListContrat(string idPartenaire)
        {
            List<Contrat> listContrat = contratDao.list(idPartenaire);

            if (listContrat == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                return null;
            }
            if(listContrat.Count == 0)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                return listContrat;
            }

        }
    }
}
