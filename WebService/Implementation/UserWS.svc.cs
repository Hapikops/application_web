﻿using System.ServiceModel.Web;
using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;
using BiocoldProcess.WebService.ContratWS;
using BiocoldProcess.Dao;

///
/// Auteur : Burléon Junior
/// Date de création : 03/02/2014
/// 

namespace BiocoldProcess.WebService.Implementation
{
    public class UserWS : IUserWS
    {
        private IUtilisateurDao userDao = new UtilisateurDao();

        public Utilisateur login(string login)
        {
            if(userDao.exist(login))
            {
                 Utilisateur user = userDao.find(login);
                 WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                 return user;
            }
            else
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
            
            return null;
        }
    }
}
