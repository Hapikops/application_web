﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;
using BiocoldProcess.WebService.ContratWS;
using BiocoldProcess.Dao;

///
/// Auteur : Burléon Junior
/// Date de création : 12/02/2014
/// 

namespace BiocoldProcess.WebService.Implementation
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "AgentWS" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez AgentWS.svc ou AgentWS.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    public class AgentWS : IAgentWS
    {
        private IAgentDao agentDao = new AgentDao();

        public Agent getAgent(string idUser)
        {
            Agent agent = agentDao.find(int.Parse(idUser));

            if (agent == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                return agent;
            }
        }
    }
}
