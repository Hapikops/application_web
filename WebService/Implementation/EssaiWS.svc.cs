﻿using BiocoldProcess.Dao;
using BiocoldProcess.IDao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using BiocoldProcess.Domaine;
using BiocoldProcess.WebService.ContratWS;

///
/// Auteur : Burléon Junior
/// Date de création : 21/03/2014
/// 

namespace BiocoldProcess.WebService.Implementation
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom de classe "EssaiWS" à la fois dans le code, le fichier svc et le fichier de configuration.
    // REMARQUE : pour lancer le client test WCF afin de tester ce service, sélectionnez EssaiWS.svc ou EssaiWS.svc.cs dans l'Explorateur de solutions et démarrez le débogage.
    public class EssaiWS : IEssaiWS
    {
        private IEssaiDao essaiDao = new EssaiDao();

        public List<Essai> getListEssai(string idPartenaire)
        {
            List<Essai> listEssai = essaiDao.listWithoutContract(idPartenaire);

            if (listEssai == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
                return null;
            }
            if (listEssai.Count == 0)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                return listEssai;
            }

        }


        public Essai getEssai(string idEssai)
        {
            Essai essai = essaiDao.find(int.Parse(idEssai));

            if (essai == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }
            else
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
                return essai;
            }
        }


        public int addEssai(Essai essai)
        {
            int id = -1;
            try
            {
                //Incrémentation du numéro d'essai du partenaire. (Numéro d'essai qui apparait pour le partenaire)
                essai.IdEssaiPartenaire = essaiDao.lastNumEssaiPart(essai.IdPartenaire) + 1;
                essaiDao.update(essai);
                //Id client est le même que celui de l'essai car ils sont dans la même table en bdd
                //Et l'on part du principe que le client est toujours créé avant l'essai et que l'essai est toujours lié à un client.
                essai.Id = essai.IdClient;
                id = essai.Id;

                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.OK;
            }
            catch
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.InternalServerError;
            }

            return id;
        }
    }
}
