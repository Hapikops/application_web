﻿// auteur: frederic Radigoy
// réalisé le 19/02/2014
// Utilisation de google maps api v3
// Modification le 20/02/2014

function initialisation(){

	   var latlng = new google.maps.LatLng(45.3370518, 1.9005528); 
	   var options = {
		   center: latlng,
			zoom: 5,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	   };  
	var carte = new google.maps.Map(document.getElementById("map"), options);
	// liste des marqueurs à afficher
	var listeDeMarqueurs=[];

	// récupérer la liste de marqueur
	var tbody = document.getElementsByName("tbody");
	for(var i=0; i < tbody.length; i++){
		var lat;
		var lng;
		if(	tbody[i].className =="latitude")
		{
			lat = document.querySelector("td #latitude");	
		}
		else
		{
			if(tbody[i].className =="longitude")
			{
				lng = document.querySelector("td #longitude");
			}
		}
		listeDeMarqueurs.push(new google.maps.LatLng(lat,lng));
	}


	// afficher la liste de marqueurs 
	for(var marqueur in listeDeMarqueurs ){
		
		new google.maps.Marker({
			position: listeDeMarqueurs[marqueur],
			map: carte
		});
	}
}
