﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="localisation.aspx.cs" Inherits="Biocold_localisation" %>
<!--Utilisation de la Google map API
    Objectif de ce page: Afficher les localisation des clients
    Auteur: RADIGOY Frederic
    Le 18/02/2014 -->
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>localisation of Clients</title>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <link href="css/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <style>
           #map {
                   width: 500px;
                   height: 500px;
                   float: initial;
                
                }
        </style>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

</head>
<body onload="initialisation()">

        <nav class="navbar navbar-default" role="navigation" style="background-color:#0073bc;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.aspx">Biocold Process</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="#">Centrales & Syndicats Partenaires</a></li>
                <li><a href="#">Formation</a></li>
                <li><a href="localisation.aspx">localisation</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">...</a></li>
                <li class="dropdown">
                    <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#">... <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                         <li><a role="menuitem" tabindex="-1" href="#">...</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="listUser.aspx">Liste des utilisateurs</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">...</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <form id="form1" runat="server">
		  <div class="container">
				<div id="map"></div>
				<div class="panel panel-default">
					<div class="panel-heading"><h1>Liste de Clients effectues </h1></div>
					<table class="table" id="table">
						<thead>
						   <tr>
							<th>Nom de l'Entreprise</th>
							<th>longitude</th>
							<th>latitude</th>
							<th>Date de passage</th>
							<th>Heure de passage</th>
						   </tr>
						</thead>
						<tbody>
							 <tr>
								<td>Biocold process</td>
								<td id="longitude">44.8544760</td>
								<td id="latitude"> -0.5977413</td>
								<td>18/02/2014</td>
								<td>12h30</td>
							</tr>
							<tr> 
								<td>Cdiscount</td>
								<td id="longitude">44.855224</td>
								<td id="latitude">-0.595492</td>
								<td>18/02/2014</td>
								<td>13h30</td>
							</tr>
						</tbody>
					</table>
				</div>
		 </div>	
    </form>
    <script src="css/js/localisation.gmaps.js"></script>
    <script src="css/js/jquery-2.0.3.min.js"></script>
    <script src="css/js/bootstrap.min.js"></script>
</body>
</html>
