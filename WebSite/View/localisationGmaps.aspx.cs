﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WSBiocold.WebService.Domaine;
using WSBiocold.WebService.Dao;


namespace BiocoldProcess.WebSite.View
{
    public partial class localisationGmaps : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

 

            double lat, lng;
            List<ScanK7> listeDeScanK7 = new List<ScanK7>();
            List<double> listeDesCoordonnees = new List<double>();
            
           foreach( ScanK7 leScanK7 in listeDeScanK7){
                lat = leScanK7.Latitude;
                lng = leScanK7.Longitude;

                listeDesCoordonnees.Add(lat);
                listeDesCoordonnees.Add(lng);
    
            }
           var geocode = string.Join(",", listeDesCoordonnees.ToArray());
           
           ClientScript.RegisterArrayDeclaration("listeLocalisation", geocode);
        }
    }
}