﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WSBiocold.WebService.Domaine;
using WSBiocold.WebService.Dao;
using Subgurim.Controles;



namespace BiocoldProcess.WebSite.View
{
    public partial class localisationGmaps : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Creation de la localisation 
            GLatLng latLng = new GLatLng(46.4755102, 2.5206795);

            // Ajout des options permettant de zoom 
            GControl controle = new GControl(GControl.preBuilt.LargeMapControl);
            


            GMap1.Add(controle);
            GMap1.setCenter(latLng);
            GMap1.Height = 500;
            GMap1.Width = 500;
            
    

        }


        protected void btn_rechercher_Click(object sender, EventArgs e)
        {
            int idUsers = Convert.ToInt32(txtbx_idUsers.Text);
            DateTime date = Convert.ToDateTime(txtbx_date);

            if (String.IsNullOrEmpty(txtbx_date.Text) && String.IsNullOrEmpty(txtbx_idUsers.Text)) 
                listDeScan();
            else
            {
                if(String.IsNullOrEmpty(txtbx_date.Text))
                   listeDeScanK7(idUsers);
                else
                   laListeDeScanK7(idUsers, date);

            }
        }

        protected void listeDeScanK7(int p_idUsers)
        {
            ScanK7Dao lesScanK7 = new ScanK7Dao();
            List<ScanK7> listDeScanK7 = lesScanK7.list(p_idUsers);
            List<GMarker> lesMarqueurs = new List<GMarker>();

            foreach (ScanK7 leScanK7 in listDeScanK7)
            {
                GMarker leMarqueur = new GMarker(new GLatLng(leScanK7.Latitude, leScanK7.Longitude));

                lesMarqueurs.Add(leMarqueur);

            }
            GMap1.markerClusterer = new MarkerClusterer(lesMarqueurs);


        }
        protected void listDeScan()
        {
            ScanK7Dao lesScanK7 = new ScanK7Dao();
            List<ScanK7> listeDeScanK7 = lesScanK7.liste();
            List<GMarker> lesMarquers = new List<GMarker>();

            foreach (ScanK7 leScanK7 in listeDeScanK7)
            {
                GMarker leMarqueur = new GMarker(new GLatLng(leScanK7.Latitude, leScanK7.Longitude));

                lesMarquers.Add(leMarqueur);

            }
         }
        protected void laListeDeScanK7(int p_idUsers, DateTime p_date)
        {
            ScanK7Dao lesScanK7 = new ScanK7Dao();
            List<ScanK7> listeDeScanK7 = lesScanK7.listScan(p_idUsers, p_date);
            List<GMarker> lesMarqueurs = new List<GMarker>();

            foreach (ScanK7 leScanK7 in listeDeScanK7)
            {

                GMarker leMarqueur = new GMarker(new GLatLng(leScanK7.Latitude, leScanK7.Longitude));

                lesMarqueurs.Add(leMarqueur);

            }
            GMap1.markerClusterer = new MarkerClusterer(lesMarqueurs);

        }
    }
}