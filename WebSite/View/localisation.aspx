﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="localisation.aspx.cs" Inherits="BiocoldProcess.WebSite.View.localisationGmaps" %>
<%@ register assembly="GMaps" namespace="Subgurim.Controles" tagprefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Localisation scanK7</title>
    <link href="../css/css/bootstrap.css" rel="stylesheet" type="text/css" />
</head>
<body>
     <nav class="navbar navbar-default" role="navigation" style="background-color: #0073bc;">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.aspx">Biocold Process</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="#">Centrales & Syndicats Partenaires</a></li>
                <li><a href="#">Formation</a></li>
                <li><a href="localisation.aspx">localisation</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">...</a></li>
                <li class="dropdown">
                    <a id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#">... <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                        <li><a role="menuitem" tabindex="-1" href="#">...</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="listUser.aspx">Liste des utilisateurs</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#">...</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <div class="container">
            <h1>Localisation des ScanK7:</h1>
            <form id="txtIdUsers" runat="server" role="form">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Saisir l'IdUtilisateur: </label>
                    <asp:TextBox ID="txtbx_idUsers" runat="server" TextMode="SingleLine"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label"> Saisir une date: </label> 
                    <asp:TextBox ID="txtbx_date" runat="server" CssClass="form-group" Text="date" TextMode="DateTime"></asp:TextBox>
                </div>
                <asp:Button ID="btn_rechercher" runat="server" Text="recherche" OnClick="btn_rechercher_Click" />
                <cc1:GMap ID="GMap1" runat="server"/>
            </form>
    </div>
    
    <script src="css/js/jquery-2.0.3.min.js"></script>
    <script src="css/js/bootstrap.min.js"></script>
</body>
</html>
