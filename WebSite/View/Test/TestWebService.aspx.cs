﻿using BiocoldProcess.WebService.ContratWS;
using BiocoldProcess.WebService.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WSBiocold;

namespace BiocoldProcess.WebSite.View.Test
{
    public partial class TestWebService : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = "";
            PassageAgentWS passageAgentWS = new PassageAgentWS();
            var passage = new ContractDataPassageAgent();

            passage.IdUser = Request.Form["TextBox1"];
            passage.Date = Request.Form["TextBox2"];
            passage.IdAgent = Request.Form["TextBox3"];
            passage.IdEssai = Request.Form["TextBox4"];

            passageAgentWS.faireUnPassage(passage);

            if (passage.IdUser != null)
                Label1.Text = passageAgentWS.faireUnPassage(passage).ToString();
        }
    }
}