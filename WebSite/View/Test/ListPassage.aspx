﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebSite/View/masterPage.Master" AutoEventWireup="true" CodeBehind="ListPassage.aspx.cs" Inherits="BiocoldProcess.WebSite.View.ListPassage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <form runat="server" name="form1">
        <h1>Liste des passage</h1>

        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ListPassageAgent" AllowPaging="True">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
                <asp:BoundField DataField="IdEssai" HeaderText="IdEssai" SortExpression="IdEssai" />
                <asp:BoundField DataField="IdAgent" HeaderText="IdAgent" SortExpression="IdAgent" />
                <asp:BoundField DataField="DatePassage" HeaderText="DatePassage" SortExpression="DatePassage" />
                <asp:BoundField DataField="IdUtilisateur" HeaderText="IdUtilisateur" SortExpression="IdUtilisateur" />
                <asp:BoundField DataField="TypePassage" HeaderText="TypePassage" SortExpression="TypePassage" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="ListPassageAgent" runat="server" SelectMethod="list" TypeName="WSBiocold.WebService.Dao.PassageAgentDao"></asp:ObjectDataSource>
    </form>
</asp:Content>
