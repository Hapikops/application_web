﻿using BiocoldProcess.Dao;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BiocoldProcess.WebSite.View.Test
{
    public partial class ScriptSQL : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            OleDbConnection oleDbConnection_BCP = ConnectionBCP.getConnection();

            //Query Sql
            //string query = "ALTER TABLE tbl_type_vis_part ADD Categorie INTEGER";
            //string query = "DELETE FROM tblPassageAgent WHERE IdUtilSaisie = ;";
            string query = "";

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);

            //Ouverture de la connection
            oleDbConnection_BCP.Open();

            try
            {
                //Exécution du Query
                oleDbCommand.ExecuteReader();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                //Ouverture de la connection
                oleDbConnection_BCP.Close();
            }
        }
    }
}