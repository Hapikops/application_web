﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebSite/View/masterPage.Master" AutoEventWireup="true" CodeBehind="ListScan.aspx.cs" Inherits="BiocoldProcess.WebSite.View.Test.ListScan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h1>Liste des scan en fonction d&#39;un partenaire</h1>
    <form runat="server" name="form1">
        <asp:TextBox ID="txt_idPartenaire" runat="server"></asp:TextBox>
        <asp:Button class="btn btn-success" ID="btn_Envoyer" runat="server" Text="Envoyer" />

        <asp:GridView ID="gv_ListScanK7" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="ListScanK7">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
                <asp:BoundField DataField="Code" HeaderText="Code" SortExpression="Code" />
                <asp:BoundField DataField="IdUser" HeaderText="IdUser" SortExpression="IdUser" />
                <asp:BoundField DataField="Longitude" HeaderText="Longitude" SortExpression="Longitude" />
                <asp:BoundField DataField="Latitude" HeaderText="Latitude" SortExpression="Latitude" />
                <asp:BoundField DataField="DateSaisie" HeaderText="DateSaisie" SortExpression="DateSaisie" />
                <asp:BoundField DataField="IdPassageAgent" HeaderText="IdPassageAgent" SortExpression="IdPassageAgent" />
                <asp:BoundField DataField="EtatK7" HeaderText="EtatK7" SortExpression="EtatK7" />
                <asp:BoundField DataField="EtatDate" HeaderText="EtatDate" SortExpression="EtatDate" />
                <asp:BoundField DataField="IdEssai" HeaderText="IdEssai" SortExpression="IdEssai" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="ListScanK7" runat="server" SelectMethod="list" TypeName="WSBiocold.WebService.Dao.ScanK7Dao">
            <SelectParameters>
                <asp:ControlParameter ControlID="txt_idPartenaire" Name="idUtilisateur" PropertyName="Text" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </form>
</asp:Content>
