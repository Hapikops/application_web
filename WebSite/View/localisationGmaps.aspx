﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="localisationGmaps.aspx.cs" Inherits="BiocoldProcess.WebSite.View.localisationGmaps" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Localisation scanK7</title>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&language=fr"></script>
    <script type="text/javascript">
        function initialisation() {
            var latlng = new google.maps.LatLng(46.2276380, 2.2137490);
            var options = {
                center: latlng,
                zoom: 5,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var carte = new google.maps.Map(document.getElementById("Map"), options);
            

            for (var i = 0; i < listeLocalisation.length; i++) {
                var args = ListeLocalisat[i].split(",");
                var location = new google.maps.LatLng(args[0], args[1])
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
            }
        }
    </script>
</head>
<body onload="initialisation()">
    <form id="form1" runat="server">
        <div id="Map" style="width: 600px; height: 390px;"></div>
    </form>
    <script src="js/localisation.gmaps.js"></script>
    <script src="css/js/jquery-2.0.3.min.js"></script>
    <script src="css/js/bootstrap.min.js"></script>
</body>
</html>
