﻿using System.Data.OleDb;
using BiocoldProcess.Domaine;
using WSBiocold.IDao;

///
/// Auteur : Burléon Junior
/// Date de création : 30/01/2014
/// 

namespace BiocoldProcess.Dao
{
    /// <summary>
    /// Cette classe permet de manager la table tblPartenaire avec les DB MS Access
    /// Les informations pouvant être manager ne sont que celles définient par la classe Partenaire
    /// <seealso cref="Domaine.Partenaire"/>
    /// </summary>
    public class PartenaireDao : IPartenaireDao
    {
        /**
         * Pour récupérer la connection à la DB MS Access BCP
         * */
        private OleDbConnection oleDbConnection_BCP = ConnectionBCP.getConnection();

        /**
         * OleDateReader est un objet récupérant les informations retourné par la requête. Cette objet est en lecture seul.
         * */
        OleDbDataReader oleDbReader = null;

        public Partenaire find(string initial)
        {
            Partenaire partenaire = null;

            //Query Sql
            string query = "SELECT InitialesPartenaire, NomPartenaire, PrénomPartenaire, AdrPart, CodePostPart, VillePart, DateNaissPart, TelPart1, TelPart2, TélPortPart, EmailPart, tel_part_fact, TypePart, bareme_com "
                         + "FROM tblPartenaire "
                         + "WHERE InitialesPartenaire = ? ";

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@InitialesPartenaire", initial);

            //Ouverture de la connection
            oleDbConnection_BCP.Open();

            try
            {
                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                //Si on a bien récupéré un record on hydrate un objet Partenaire
                while (oleDbReader.Read())
                {
                    if (oleDbReader["InitialesPartenaire"].ToString() != null)
                    {
                        partenaire = new Partenaire();
                        partenaire.Id = oleDbReader["InitialesPartenaire"].ToString();
                        partenaire.Nom = oleDbReader["NomPartenaire"].ToString();
                        partenaire.Prenom = oleDbReader["PrénomPartenaire"].ToString();
                        partenaire.Adresse = oleDbReader["AdrPart"].ToString();
                        partenaire.CodePostal = oleDbReader["CodePostPart"].ToString();
                        partenaire.Ville = oleDbReader["VillePart"].ToString();
                        if (oleDbReader["DateNaissPart"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                            partenaire.DateNaissance = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateNaissPart"));
                        partenaire.Telephone1 = oleDbReader["TelPart1"].ToString();
                        partenaire.Telephone2 = oleDbReader["TelPart2"].ToString();
                        partenaire.TelephonePortable = oleDbReader["TélPortPart"].ToString();
                        partenaire.Email = oleDbReader["EmailPart"].ToString();
                        if (oleDbReader["TypePart"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                            partenaire.Type = oleDbReader.GetInt32(oleDbReader.GetOrdinal("TypePart"));
                        partenaire.TelephoneFacture = oleDbReader["tel_part_fact"].ToString();
                        if (oleDbReader["bareme_com"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                            partenaire.BaremeCom = oleDbReader.GetInt16(oleDbReader.GetOrdinal("bareme_com"));
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                //Fermeture du lecteur
                oleDbReader.Close();
                //Fermeture de la connection
                oleDbConnection_BCP.Close();
            }

            return partenaire;
        }

        public void delete(string initial)
        {
            throw new System.NotImplementedException();
        }

        public void add(Partenaire partenaire)
        {
            throw new System.NotImplementedException();
        }

        public void update(Partenaire partenaire)
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.List<Partenaire> list()
        {
            throw new System.NotImplementedException();
        }
    }
}