﻿using BiocoldProcess.Tools;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;

///
/// Auteur : Burléon Junior
/// Date de création : 04/02/2014
/// 

namespace BiocoldProcess.Dao
{
    /// <summary>
    /// Cette classe permet de manager la table tblUtilisateurs avec les DB MS Access
    /// <seealso cref="Domaine.Utilisateur"/>
    /// </summary>
    public class UtilisateurDao : IUtilisateurDao
    {
        /**
         * Pour récupérer la connection à la DB MS Access BCP
         * */
        private OleDbConnection oleDbConnection_BCP = ConnectionBCP.getConnection();

        /**
         * OleDateReader est un objet récupérant les informations retourné par la requête. Cette objet est en lecture seul.
         * */
        OleDbDataReader oleDbReader = null;

        public Utilisateur find(string login)
        {
            Utilisateur user = null;

            //Query Sql
            string query = "SELECT * "
                         + "FROM tblUtilisateurs "
                         + "WHERE nom_util = ? ";

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@nom_util", login);

            //Ouverture de la connection
            oleDbConnection_BCP.Open();



            try
            {
                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                //Si on a bien récupéré un record on hydrate un objet Utilisateur
                while (oleDbReader.Read())
                {
                    user = new Utilisateur();
                    if (oleDbReader["id_util"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        user.Id = oleDbReader.GetInt32(oleDbReader.GetOrdinal("id_util"));
                    user.Login = oleDbReader["nom_util"].ToString();
                    user.Password = Utility.ConvertMD5(oleDbReader["mopass_util"].ToString());
                    if (oleDbReader["niveau_util"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        user.NiveauAcces = oleDbReader.GetInt16(oleDbReader.GetOrdinal("niveau_util"));
                    if (oleDbReader["date_creation_util"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        user.DateCreation = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("date_creation_util"));
                    user.IdPartenaire = oleDbReader["id_part"].ToString();
                    if (oleDbReader["maintenance"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        user.Maintenance = oleDbReader.GetBoolean(oleDbReader.GetOrdinal("maintenance"));
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                //Fermeture du lecteur
                oleDbReader.Close();
                //Fermeture de la connection
                oleDbConnection_BCP.Close();
            }

            return user;
        }

        public Domaine.Utilisateur find(int id)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public void add(Domaine.Utilisateur utilisateur)
        {
            throw new NotImplementedException();
        }

        public void update(Domaine.Utilisateur utilisateur)
        {
            throw new NotImplementedException();
        }

        public List<Domaine.Utilisateur> list()
        {
            throw new NotImplementedException();
        }

        public bool exist(string login)
        {
            //Query Sql
            string query = "SELECT nom_util "
                         + "FROM tblUtilisateurs "
                         + "WHERE nom_util = ? ";

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@nom_util", login);

            //Ouverture de la connection
            oleDbConnection_BCP.Open();

            try
            {
                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                //Si on a bien récupéré un record on retour true, sinon false.
                if (oleDbReader.Read())
                {
                    //Fermeture du lecteur
                    oleDbReader.Close();
                    //Fermeture de la connection
                    oleDbConnection_BCP.Close();
                    return true;
                }
                else
                {
                    //Fermeture du lecteur
                    oleDbReader.Close();
                    //Fermeture de la connection
                    oleDbConnection_BCP.Close();
                    return false;
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                //Fermeture du lecteur
                oleDbReader.Close();
                //Fermeture de la connection
                oleDbConnection_BCP.Close();
            }
        }
    }
}