﻿using BiocoldProcess.IDao;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 21/03/2014
/// 

namespace BiocoldProcess.Dao
{
    /// <summary>
    /// Cette classe permet de manager la table tblessaicontrat avec la DB MS Access, en se focalisant sur les information d'un essai lui même
    /// Les informations pouvant être manager ne sont que celles définient par la classe Essai
    /// <seealso cref="Domaine.Essai"/>
    /// </summary>
    public class EssaiDao : IEssaiDao
    {
        /**
         * Pour récupérer la connection à la DB MS Access BCP
         * */
        private OleDbConnection oleDbConnection_BCP = ConnectionBCP.getConnection();

        /**
         * OleDateReader est un objet récupérant les informations retourné par la requête. Cette objet est en lecture seul.
         * */
        OleDbDataReader oleDbReader = null;

        public List<Essai> listWithoutContract(string idPartenaire)
        {
            List<Essai> listEssai = null;

            //Query Sql
            string query = "SELECT InitialesPartenaire, DateDernPass, DateDernChgt, N°Essai, K7retirees1erRdvSign, K7retirees2emeRdvSign, Result3emeRdv, NumContrat, NumEssaiPart, DateDebutEssai "
                         + "FROM tblessaicontrat "
                         + "WHERE InitialesPartenaire = ? AND NumContrat Is Null "; 

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@InitialesPartenaire", idPartenaire);

            //Ouverture de la connection
            oleDbConnection_BCP.Open();

            //Exécution du Query
            oleDbReader = oleDbCommand.ExecuteReader();

            try
            {
                listEssai = new List<Essai>();

                //Si on a bien récupéré un record on hydrate un objet Partenaire
                while (oleDbReader.Read())
                {
                    int numContrat = 0;
                    Essai essai = new Essai();
                    essai.IdPartenaire = oleDbReader["InitialesPartenaire"].ToString();
                    if (oleDbReader["DateDernChgt"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        essai.DateDernierChangement = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateDernChgt"));
                    if (oleDbReader["DateDernPass"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        essai.DateDernierPassage = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateDernPass"));
                    if (oleDbReader["DateDebutEssai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        essai.DateDebut = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateDebutEssai"));
                    if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        essai.IdClient = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                    if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        essai.Id = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                    if (oleDbReader["NumEssaiPart"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        essai.IdEssaiPartenaire = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumEssaiPart"));
                    if (oleDbReader["NumContrat"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        numContrat = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumContrat"));

                    

                    if (oleDbReader.GetBoolean(oleDbReader.GetOrdinal("K7retirees1erRdvSign")) == false && oleDbReader.GetBoolean(oleDbReader.GetOrdinal("K7retirees2emeRdvSign")) == false && oleDbReader.GetInt32(oleDbReader.GetOrdinal("Result3emeRdv")) != 2 && numContrat == 0)
                        essai.Completed = false;
                    else
                        essai.Completed = true;

                    listEssai.Add(essai);
                }
            }
            catch (System.Exception)
            {
                listEssai = null;
                throw;
            }
            finally
            {
                //Fermeture du lecteur
                if (oleDbReader != null)
                    oleDbReader.Close();
                //Fermeture de la connection
                if (oleDbConnection_BCP != null)
                    oleDbConnection_BCP.Close();
            }
            return listEssai;
        }

        public Essai find(int id)
        {
            Essai essai = null;

            //Query Sql
            string query = "SELECT InitialesPartenaire, DateDernPass, DateDernChgt, N°Essai, K7retirees1erRdvSign, K7retirees2emeRdvSign, Result3emeRdv, NumContrat, NumEssaiPart, DateDebutEssai "
                         + "FROM tblessaicontrat "
                         + "WHERE N°Essai = ? ";

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@N°Essai", id);

            try
            {
                //Ouverture de la connection
                oleDbConnection_BCP.Open();

                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                //Si on a bien récupéré un record on hydrate un objet Partenaire
                while (oleDbReader.Read())
                {
                    int numContrat = 0;
                    essai = new Essai();
                    essai.IdPartenaire = oleDbReader["InitialesPartenaire"].ToString();
                    if (oleDbReader["DateDernChgt"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        essai.DateDernierChangement = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateDernChgt"));
                    if (oleDbReader["DateDernPass"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        essai.DateDernierPassage = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateDernPass"));
                    if (oleDbReader["DateDebutEssai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        essai.DateDebut = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateDebutEssai"));
                    if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        essai.IdClient = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                    if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        essai.Id = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                    if (oleDbReader["NumEssaiPart"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        essai.IdEssaiPartenaire = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumEssaiPart"));
                    if (oleDbReader["NumContrat"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        numContrat = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumContrat"));

                    if (oleDbReader.GetBoolean(oleDbReader.GetOrdinal("K7retirees1erRdvSign")) == false && oleDbReader.GetBoolean(oleDbReader.GetOrdinal("K7retirees2emeRdvSign")) == false && oleDbReader.GetInt32(oleDbReader.GetOrdinal("Result3emeRdv")) != 2 && numContrat == 0)
                        essai.Completed = false;
                    else 
                        essai.Completed = true;
                }
            }
            catch (System.FormatException)
            {
                return essai;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                //Fermeture du lecteur
                if (oleDbReader != null)
                    oleDbReader.Close();
                //Fermeture de la connection
                if (oleDbConnection_BCP != null)
                    oleDbConnection_BCP.Close();
            }
            return essai;
        }

        public void update(Essai essai)
        {
            string query = "UPDATE tblessaicontrat "
                         + "SET DateDebutEssai = ? , DateDernPass = ? , InitialesPartenaire = ? , NumEssaiPart = ? , DateDernChgt = ? "
                         + "WHERE N°Essai = ? ";

            //Objet permettant de préparer une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@DateDebutEssai", essai.DateDebut);
            oleDbCommand.Parameters.AddWithValue("@DateDernPass", essai.DateDernierPassage);
            oleDbCommand.Parameters.AddWithValue("@InitialesPartenaire", essai.IdPartenaire);
            oleDbCommand.Parameters.AddWithValue("@NumEssaiPart", essai.IdEssaiPartenaire);
            oleDbCommand.Parameters.AddWithValue("@DateDernChgt", essai.DateDernierChangement);
            oleDbCommand.Parameters.AddWithValue("@N°Essai", essai.DateDernierChangement);

            //Ouverture de la connection
            oleDbConnection_BCP.Open();

            try
            {
                //Exécution du query
                oleDbCommand.ExecuteNonQuery();
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                //Fermeture de la connection
                oleDbConnection_BCP.Close();
            }
        }

        public int lastNumEssaiPart(string idPartenaire)
        {
            int lastNumEssaiPart = 0;

            //Query Sql
            string query = "SELECT TOP 1 NumEssaiPart "
                         + "FROM tblessaicontrat "
                         + "WHERE InitialesPartenaire = ? "
                         + "ORDER BY NumEssaiPart DESC ";

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@InitialesPartenaire", idPartenaire);

            try
            {
                //Ouverture de la connection
                oleDbConnection_BCP.Open();

                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                //Si on a bien récupéré un résultat.
                if(oleDbReader.Read())
                    lastNumEssaiPart = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumEssaiPart"));
                
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                //Fermeture du lecteur
                if (oleDbReader != null)
                    oleDbReader.Close();
                //Fermeture de la connection
                if (oleDbConnection_BCP != null)
                    oleDbConnection_BCP.Close();
            }

            return lastNumEssaiPart;
        }
    }
}