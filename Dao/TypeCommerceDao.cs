﻿using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;

///
/// Auteur : Burléon Junior
/// Date de création : 03/04/2014
/// 

namespace BiocoldProcess.Dao
{
    /// <summary>
    /// Cette classe permet de manager la table tbl_TYPE_COMMERCE avec les DB MS Access
    /// Les informations pouvant être manager ne sont que celles définient par la classe TypeCommerce
    /// <seealso cref="Domaine.TypeCommerce"/>
    /// </summary>
    public class TypeCommerceDao : ITypeCommerceDao
    {
        /*
         * Pour établir le connexion à la DB MS Access BCP
         */
        private OleDbConnection oleDbConnection_BCP = ConnectionBCP.getConnection();

        /**
         * OleDateReader est un objet récupérant les informations retourné par la requête. Cette objet est en lecture seul.
         * */
        OleDbDataReader oleDbReader = null;


        public List<TypeCommerce> list()
        {
            List<TypeCommerce> liste = new List<TypeCommerce>();

            // SqlQuery
            string sqlQuery = "SELECT numauto_typcom, TYPE_COMMERCE "
                             + "FROM tbl_TYPE_COMMERCE ";

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(sqlQuery, oleDbConnection_BCP);

            try
            {
                //Ouverture de la connection
                oleDbConnection_BCP.Open();

                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                while (oleDbReader.Read())
                {
                    TypeCommerce typeCommerce = new TypeCommerce();
                    if (oleDbReader["numauto_typcom"].ToString() != "")
                        typeCommerce.Id = oleDbReader.GetInt32(oleDbReader.GetOrdinal("numauto_typcom"));
                    if (oleDbReader["TYPE_COMMERCE"].ToString() != "")
                        typeCommerce.Value = oleDbReader.GetString(oleDbReader.GetOrdinal("TYPE_COMMERCE"));

                    liste.Add(typeCommerce);
                }

            }
            catch (System.Exception)
            {
                liste = null;
                throw;

            }
            finally
            {
                //Fermeture du lecteur
                if (oleDbReader != null)
                    oleDbReader.Close();
                //Fermeture de la connection
                if (oleDbConnection_BCP != null)
                    oleDbConnection_BCP.Close();
            }
            return liste;
        }
    }
}