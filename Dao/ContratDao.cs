﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;

///
/// Auteur : Burléon Junior
/// Date de création : 11/02/2014
/// 

namespace BiocoldProcess.Dao
{
    /// <summary>
    /// Cette classe permet de manager la table tblessaicontrat avec la DB MS Access, en se focalisant sur les information d'un contrat lui même
    /// Les informations pouvant être manager ne sont que celles définient par la classe Contrat
    /// <seealso cref="Domaine.Contrat"/>
    /// </summary>
    public class ContratDao : IContratDao
    {
        /**
         * Pour récupérer la connection à la DB MS Access BCP
         * */
        private OleDbConnection oleDbConnection_BCP = ConnectionBCP.getConnection();

        /**
         * OleDateReader est un objet récupérant les informations retourné par la requête. Cette objet est en lecture seul.
         * */
        OleDbDataReader oleDbReader = null;

        public Contrat find(string id)
        {
            Contrat contrat = null;

            //Query Sql
            string query = "SELECT InitialesPartenaire, NumContrat, DateDernPass, DateDernChgt, N°Essai, etatcont "
                         + "FROM tblessaicontrat "
                         + "WHERE InitialesPartenaire = ? AND NumContrat = ? ";

            //Préparation des variables
            //Les 4 instructions qui suivent permette de couper en deux l'id du contrat pour récupéré l'ID du partenaire et le numéro de contrat
            try
            {
                char[] delimiterChars = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };
                string[] words = id.Split(delimiterChars);

                string idPartenaire = words[0];
                int numeroContrat = int.Parse(id.Substring(idPartenaire.Length));

                //Objet permettant d'exécuter une commande SQL sur une DB MS Access
                OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
                oleDbCommand.Parameters.AddWithValue("@InitialesPartenaire", idPartenaire);
                oleDbCommand.Parameters.AddWithValue("@NumContrat", numeroContrat);

                //Ouverture de la connection
                oleDbConnection_BCP.Open();

                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                //Si on a bien récupéré un record on hydrate un objet Partenaire
                while (oleDbReader.Read())
                {
                    contrat = new Contrat();
                    contrat.IdPartenaire = oleDbReader["InitialesPartenaire"].ToString();
                    if (oleDbReader["NumContrat"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.NumeroContrat = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumContrat"));
                    if (oleDbReader["DateDernChgt"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.DateDernierChangement = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateDernChgt"));
                    if (oleDbReader["DateDernPass"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.DateDernierPassage = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateDernPass"));
                    if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.IdClient = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                    if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.IdEssai = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                    if (oleDbReader["etatcont"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.Etat = oleDbReader.GetInt16(oleDbReader.GetOrdinal("etatcont"));

                }
            }
            catch (System.FormatException)
            {
                return contrat;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                //Fermeture du lecteur
                if (oleDbReader != null)
                    oleDbReader.Close();
                //Fermeture de la connection
                if (oleDbConnection_BCP != null)
                    oleDbConnection_BCP.Close();
            }
            return contrat;
        }


        public void update(Contrat contrat)
        {
            string query = "UPDATE tblessaicontrat "
                         + "SET InitialesPartenaire = ? , NumContrat = ? , DateDernPass = ? , DateDernChgt = ? , etatcont = ? "
                         + "WHERE N°Essai = ? ";

            //Objet permettant de préparer une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@InitialesPartenaire", contrat.IdPartenaire);
            oleDbCommand.Parameters.AddWithValue("@NumContrat", contrat.NumeroContrat);
            oleDbCommand.Parameters.AddWithValue("@DateDernPass", contrat.DateDernierPassage);
            oleDbCommand.Parameters.AddWithValue("@DateDernChgt", contrat.DateDernierChangement);
            oleDbCommand.Parameters.AddWithValue("@etatcont", contrat.Etat);
            oleDbCommand.Parameters.AddWithValue("@N°Essai", contrat.IdEssai);

            try
            {

                //Ouverture de la connection
                oleDbConnection_BCP.Open();

                //Exécution du query
                oleDbCommand.ExecuteNonQuery();

            }
            catch (System.Exception)
            {

                throw;
            }
            finally
            {

                //Fermeture de la connection
                oleDbConnection_BCP.Close();
            }
        }

        public List<Contrat> list()
        {
            throw new NotImplementedException();
        }

        public List<Contrat> list(string idPartenaire)
        {
            List<Contrat> listContrat = null;

            //Query Sql
            string query = "SELECT InitialesPartenaire, NumContrat, DateDernChgt, DateDernPass, N°Essai, etatcont "
                         + "FROM tblessaicontrat "
                         + "WHERE InitialesPartenaire = ? AND NumContrat > 750 "
                         + "ORDER BY NumContrat ASC ";


            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@InitialesPartenaire", idPartenaire);

            try
            {
                //Ouverture de la connection
                oleDbConnection_BCP.Open();

                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                listContrat = new List<Contrat>();

                //Si on a bien récupéré un record on hydrate un objet Partenaire
                while (oleDbReader.Read())
                {
                    Contrat contrat = new Contrat();
                    contrat.IdPartenaire = oleDbReader["InitialesPartenaire"].ToString();
                    if (oleDbReader["NumContrat"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.NumeroContrat = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumContrat"));
                    if (oleDbReader["DateDernChgt"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.DateDernierChangement = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateDernChgt"));
                    if (oleDbReader["DateDernPass"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.DateDernierPassage = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateDernPass"));
                    if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.IdClient = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                    if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.IdEssai = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                    if (oleDbReader["etatcont"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.Etat = oleDbReader.GetInt16(oleDbReader.GetOrdinal("etatcont"));

                    listContrat.Add(contrat);
                }
            }
            catch (System.Exception)
            {
                listContrat = null;
                throw;
            }
            finally
            {
                //Fermeture du lecteur
                if (oleDbReader != null)
                    oleDbReader.Close();
                //Fermeture de la connection
                if (oleDbConnection_BCP != null)
                    oleDbConnection_BCP.Close();
            }
            return listContrat;
        }


        public Contrat find(int idEssai)
        {
            Contrat contrat = null;

            //Query Sql
            string query = "SELECT InitialesPartenaire, NumContrat, DateDernPass, DateDernChgt, N°Essai, etatcont "
                         + "FROM tblessaicontrat "
                         + "WHERE N°Essai = ? ";



            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@N°Essai", idEssai);

            try
            {
                //Ouverture de la connection
                oleDbConnection_BCP.Open();

                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                //Si on a bien récupéré un record on hydrate un objet Partenaire
                while (oleDbReader.Read())
                {
                    contrat = new Contrat();
                    contrat.IdPartenaire = oleDbReader["InitialesPartenaire"].ToString();
                    if (oleDbReader["NumContrat"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.NumeroContrat = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumContrat"));
                    if (oleDbReader["DateDernChgt"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.DateDernierChangement = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateDernChgt"));
                    if (oleDbReader["DateDernPass"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.DateDernierPassage = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateDernPass"));
                    if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.IdClient = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                    if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.IdEssai = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                    if (oleDbReader["etatcont"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        contrat.Etat = oleDbReader.GetInt16(oleDbReader.GetOrdinal("etatcont"));
                }
            }
            catch (System.FormatException)
            {
                return contrat;
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                //Fermeture du lecteur
                if (oleDbReader != null)
                    oleDbReader.Close();
                //Fermeture de la connection
                if (oleDbConnection_BCP != null)
                    oleDbConnection_BCP.Close();
            }
            return contrat;
        }
    }
}
