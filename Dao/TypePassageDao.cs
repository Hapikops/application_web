﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.OleDb;
using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;

///
///Auteur: frederic Radigoy
///Date de creation: 17/03/2014
///ddd
///

namespace BiocoldProcess.Dao
{
    /// <summary>
    /// Cette classe permet de manager la table tbl_type_vis_part avec  les DB MS Access
    ///<seealso cref="Domaine.TypePassage"/>
    /// </summary>

    public class TypePassageDao : ITypePassageDao
    {
        /*
         * Pour établir le connexion à la DB MS Access BCP
         */
        private OleDbConnection oleDbConnection_BCP = ConnectionBCP.getConnection();

        /**
         * OleDateReader est un objet récupérant les informations retourné par la requête. Cette objet est en lecture seul.
         * */
        OleDbDataReader oleDbReader = null;


        public void add(TypePassage leTypePassage)
        {
            throw new NotImplementedException();
        }

        public void delete(int id)
        {
            throw new NotImplementedException();
        }

        public void update(TypePassage typePassage)
        {
            throw new NotImplementedException();
        }


        public List<TypePassage> list()
        {
            List<TypePassage> liste = new List<TypePassage>();

            // SqlQuery
            string sqlQuery = "SELECT lettre_type_vis, numauto_typ_vis, type_vis, Categorie "
                             + "FROM tbl_type_vis_part "
                             + "WHERE numauto_typ_vis IN ( 1 , 5 , 6 , 7 , 8 , 27 , 28 , 29 , 30 )";

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(sqlQuery, oleDbConnection_BCP);


            try
            {
                //Ouverture de la connection
                oleDbConnection_BCP.Open();

                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                while(oleDbReader.Read())
                {
                    TypePassage typePassage = new TypePassage();
                    typePassage.Key = oleDbReader["lettre_type_vis"].ToString();
                    if (oleDbReader["numauto_typ_vis"].ToString() != "")
                        typePassage.Id = oleDbReader.GetInt32(oleDbReader.GetOrdinal("numauto_typ_vis"));
                    if (oleDbReader["type_vis"].ToString() != "")
                        typePassage.Value = oleDbReader.GetString(oleDbReader.GetOrdinal("type_vis"));
                    if (oleDbReader["Categorie"].ToString() != "")
                        typePassage.Categorie = oleDbReader.GetInt32(oleDbReader.GetOrdinal("Categorie"));

                    liste.Add(typePassage);
                }
                
            }
            catch(System.Exception)
            {
                liste = null;
                throw;

            }
            finally
            {
                //Fermeture du lecteur
                if (oleDbReader != null)
                    oleDbReader.Close();
                //Fermeture de la connection
                if (oleDbConnection_BCP != null)
                    oleDbConnection_BCP.Close();
            }
            return liste;
        }
    }
}