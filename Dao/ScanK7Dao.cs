﻿using System.Data.OleDb;
using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;
using System.Collections.Generic;

///
/// Auteur : Burléon Junior
/// Date de création : 04/02/2014
/// 
/// Auteur de Modifiction: radigoy frederic
/// Date de modification: 03/03/2014
/// 

namespace BiocoldProcess.Dao
{
    /// <summary>
    /// Cette classe permet de manager la table tblUtilisateurs avec les DB MS Access
    /// <seealso cref="Domaine.ScanK7"/>
    /// </summary>
    public class ScanK7Dao : IScanK7Dao
    {
        /**
         * Pour récupérer la connection à la DB MS Access BCP_LP
         * */
        private OleDbConnection oleDbConnection_BCP_LP = ConnectionBCP_LP.getConnection();

        /**
        * OleDateReader est un objet récupérant les informations retourné par la requête. Cette objet est en lecture seul.
        * */
        OleDbDataReader oleDbReader = null;

        public void add(ScanK7 scanK7)
        {
            //Query Sql
            string query = "INSERT INTO tblEtatActuelK7 "
                         + "(numK7, etatK7, datedepartEtatK7, idpart, numess, idSaisie, dateSaisie, NumChgt, Latitude, Longitude) "
                         + "VALUES ( ? , ? , ? , ? , ? , ? , ? , ?, ?, ? )";


            //Objet permettant de préparer une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP_LP);
            oleDbCommand.Parameters.AddWithValue("@numK7", scanK7.Code);
            oleDbCommand.Parameters.AddWithValue("@etatK7", scanK7.EtatK7);
            oleDbCommand.Parameters.AddWithValue("@datedepartEtatK7", scanK7.EtatDate);
            oleDbCommand.Parameters.AddWithValue("@idpart", scanK7.IdUser.ToString());
            oleDbCommand.Parameters.AddWithValue("@numess", scanK7.IdEssai);
            oleDbCommand.Parameters.AddWithValue("@idSaisie", scanK7.IdUser);
            oleDbCommand.Parameters.AddWithValue("@dateSaisie", scanK7.DateSaisie);
            oleDbCommand.Parameters.AddWithValue("@NumChgt", scanK7.IdPassageAgent);
            oleDbCommand.Parameters.AddWithValue("@latitude", scanK7.Latitude);
            oleDbCommand.Parameters.AddWithValue("@longitude", scanK7.Longitude);

            //Ouverture de la connection
            oleDbConnection_BCP_LP.Open();

            try
            {
                //Exécution du query
                oleDbCommand.ExecuteNonQuery();
            }
            catch (System.Exception)
            {

                throw;
            }
            finally
            {
                //Fermeture de la connection
                oleDbConnection_BCP_LP.Close();
            }

        }

        #region liste de ScanK7 selon l'IdUsers
        public List<ScanK7> list(int idUtilisateur)
        {

            List<ScanK7> listScanK7 = null;

            //Query Sql
            string query = "SELECT * "
                         + "FROM tblEtatActuelK7 "
                         + "WHERE idpart = ? ";


            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP_LP);
            oleDbCommand.Parameters.AddWithValue("@idpart", idUtilisateur);

            //Ouverture de la connection
            oleDbConnection_BCP_LP.Open();

            //Exécution du Query
            oleDbReader = oleDbCommand.ExecuteReader();

            try
            {
                listScanK7 = new List<ScanK7>();

                //Si on a bien récupéré un record on hydrate un objet Partenaire
                while (oleDbReader.Read())
                {
                    ScanK7 scanK7 = new ScanK7();
                    scanK7.Code = oleDbReader["numK7"].ToString();
                    if (oleDbReader["numautoEtatActuelK7"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        scanK7.Id = oleDbReader.GetInt32(oleDbReader.GetOrdinal("numautoEtatActuelK7"));
                    if (oleDbReader["idpart"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        scanK7.IdUser = oleDbReader.GetInt16(oleDbReader.GetOrdinal("idpart"));
                    if (oleDbReader["Longitude"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        scanK7.Longitude = oleDbReader.GetDouble(oleDbReader.GetOrdinal("Longitude"));
                    if (oleDbReader["Latitude"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        scanK7.Latitude = oleDbReader.GetDouble(oleDbReader.GetOrdinal("Latitude"));
                    if (oleDbReader["dateSaisie"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        scanK7.DateSaisie = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("dateSaisie"));
                    if (oleDbReader["NumChgt"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        scanK7.IdPassageAgent = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumChgt"));
                    if (oleDbReader["etatK7"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        scanK7.EtatK7 = oleDbReader.GetInt16(oleDbReader.GetOrdinal("etatK7"));
                    if (oleDbReader["datedepartEtatK7"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        scanK7.EtatDate = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("datedepartEtatK7"));
                    if (oleDbReader["numess"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        scanK7.IdEssai = oleDbReader.GetInt32(oleDbReader.GetOrdinal("numess"));

                    listScanK7.Add(scanK7);
                }
            }
            catch (System.Exception)
            {
                listScanK7 = null;
                throw;
            }
            finally
            {
                //Fermeture du lecteur
                if (oleDbReader != null)
                    oleDbReader.Close();
                //Fermeture de la connection
                if (oleDbConnection_BCP_LP != null)
                    oleDbConnection_BCP_LP.Close();
            }
            return listScanK7;
        }

        #endregion

        #region liste de ScanK7 selectionnée selon la date de saisie et IdUsers
        public List<ScanK7> listScan(int idUtilisateur, System.DateTime dateSaisie)
        {
            List<ScanK7> listeDeScanK7 = null;

            string sqlQuery = "SELECT*"
                             + "FROM tblEtatActuelK7"
                             + "WHERE idpart = ?"
                             + "AND dateSaisie= ?  ";

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(sqlQuery, oleDbConnection_BCP_LP);
            oleDbCommand.Parameters.AddWithValue("@idpart", idUtilisateur);
            oleDbCommand.Parameters.AddWithValue("@dateSaisie", dateSaisie);

            //Ouverture de la connection
            oleDbConnection_BCP_LP.Open();

            //Exécution du Query
            oleDbReader = oleDbCommand.ExecuteReader();

            try
            {
                listeDeScanK7 = new List<ScanK7>();

                while(oleDbReader.Read())
                {

                    ScanK7 leScanK7 = new ScanK7();
                    leScanK7.Code = oleDbReader["numK7"].ToString();

                    if(oleDbReader["numautoEtatActuelK7"].ToString() != "")
                        leScanK7.Id = oleDbReader.GetInt32(oleDbReader.GetOrdinal("numautoEtatActuelK7"));

                    listeDeScanK7.Add(leScanK7);


                }

            }
            catch(System.Exception)
            {
                listeDeScanK7 = null;
                throw;
            }
            finally
            {
                 //Fermeture du lecteur
                if (oleDbReader != null)
                    oleDbReader.Close();
                //Fermeture de la connection
                if (oleDbConnection_BCP_LP != null)
                    oleDbConnection_BCP_LP.Close();
            }


            return listeDeScanK7;
        }
        #endregion

        #region liste de ScanK7
        public List<ScanK7> liste()
        {
            List<ScanK7> liste = null;

           string sqlQuery = "SELECT *"
                            +"FROM tblEtatActuelK7";

           //Objet permettant d'exécuter une commande SQL sur une DB MS Access
           OleDbCommand oleDbCommand = new OleDbCommand(sqlQuery, oleDbConnection_BCP_LP);


           //Ouverture de la connection
           oleDbConnection_BCP_LP.Open();

           //Exécution du Query
           oleDbReader = oleDbCommand.ExecuteReader();


           try
           {
               liste = new List<ScanK7>();

               while (oleDbReader.Read())
               {

                   ScanK7 leScanK7 = new ScanK7();
                   leScanK7.Code = oleDbReader["numK7"].ToString();
                   if (oleDbReader["numautoEtatActuelK7"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                       leScanK7.Id = oleDbReader.GetInt32(oleDbReader.GetOrdinal("numautoEtatActuelK7"));
                   if (oleDbReader["idpart"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                      leScanK7.IdUser = oleDbReader.GetInt32(oleDbReader.GetOrdinal("idpart"));
                   if (oleDbReader["Longitude"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                       leScanK7.Longitude = oleDbReader.GetInt32(oleDbReader.GetOrdinal("Longitude"));
                   if (oleDbReader["Latitude"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                       leScanK7.Latitude = oleDbReader.GetInt32(oleDbReader.GetOrdinal("Latitude"));
                   if (oleDbReader["dateSaisie"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                       leScanK7.DateSaisie = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("dateSaisie"));
                   if (oleDbReader["NumChgt"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                       leScanK7.IdUser = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumChgt"));
                   if (oleDbReader["etatK7"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                       leScanK7.EtatK7 = oleDbReader.GetInt32(oleDbReader.GetOrdinal("etatK7"));
                   if (oleDbReader["datedepartEtatK7"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                       leScanK7.EtatDate = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("datedepartEtatK7"));
                   if (oleDbReader["numess"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                       leScanK7.IdEssai = oleDbReader.GetInt32(oleDbReader.GetOrdinal("numess"));


                   liste.Add(leScanK7);


               }

           }
           catch (System.Exception)
           {
               liste = null;
               throw;
           }
           finally
           {
               //Fermeture du lecteur
               if (oleDbReader != null)
                   oleDbReader.Close();
               //Fermeture de la connection
               if (oleDbConnection_BCP_LP != null)
                   oleDbConnection_BCP_LP.Close();
           }




            return liste;
        }
        #endregion
    }
}