﻿using System.Configuration;
using System.Data.OleDb;

///
/// Auteur : Burléon Junior
/// Date de création : 30/01/2014
/// 

namespace BiocoldProcess.Dao
{
    /// <summary>
    /// Cette classe permet la connection à la DB MS Access BCP_LP
    /// Cette classe est également un Singleton
    /// </summary>
    public class ConnectionBCP_LP
    {
        /// <summary>
        /// Les informations de connection à la database en question sont récupérer dans le fichier de config. Avec le login : BCP_LP_ConnectionDB
        /// </summary>
        static string connectionString = ConfigurationManager.ConnectionStrings["BCP_LP_ConnectionDB"].ConnectionString;
        static OleDbConnection connection = null;

        /// <summary>
        /// Permet de récupérer la connection à la DB MS Access BCP_LP
        /// </summary>
        /// <returns>Retourne la connection</returns>
        static public OleDbConnection getConnection()
        {
            if (connection == null)
                connection = new OleDbConnection(connectionString);
            return connection;
        }

        /// <summary>
        /// Privée car singleton
        /// </summary>
        private ConnectionBCP_LP()
        {

        }

        /// <summary>
        /// Le destructeur ferme la connection à la database
        /// </summary>
        ~ConnectionBCP_LP()
        {
            connection.Close();
        }
    }

}