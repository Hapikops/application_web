﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BiocoldProcess.IDao;
using BiocoldProcess.Domaine;
using System.Data.OleDb;

///
/// Auteur : Burléon Junior
/// Date de création : 12/02/2014
/// 

namespace BiocoldProcess.Dao
{
    /// <summary>
    /// Cette classe permet de manager la table tblessaiecontrat avec les DB MS Access
    /// Les informations pouvant être manager ne sont que celles définient par la classe Client
    /// <seealso cref="Domaine.Client"/>
    /// </summary>
    public class ClientDao : IClientDao
    {
        /**
         * Pour récupérer la connection à la DB MS Access BCP
         * */
        private OleDbConnection oleDbConnection_BCP = ConnectionBCP.getConnection();

        /**
         * OleDateReader est un objet récupérant les informations retourné par la requête. Cette objet est en lecture seul.
         * */
        OleDbDataReader oleDbReader = null;

        public Client find(int id)
        {
            Client client = null;

            //Query Sql
            string query = "SELECT N°Essai, NomDuCommerce, NomClientEssai, PrenomClientEssai, AdresseEssai, VilleEssai, CodePostalEssai, InitialesPartenaire, NumContrat  "
                         + "FROM tblessaicontrat "
                         + "WHERE N°Essai = ? ";

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@N°Essai", id);

            //Ouverture de la connection
            oleDbConnection_BCP.Open();

            try
            {
                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                //Si on a bien récupéré un record on hydrate un objet Partenaire
                while (oleDbReader.Read())
                {
                    if (oleDbReader["NomDuCommerce"].ToString() != null)
                    {
                        client = new Client();
                        if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                            client.Id = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                        if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                            client.IdEssai = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                        client.IdPartenaire = oleDbReader["InitialesPartenaire"].ToString();
                        client.Nom = oleDbReader["NomClientEssai"].ToString();
                        client.Prenom = oleDbReader["PrenomClientEssai"].ToString();
                        client.AdresseCommerce = oleDbReader["AdresseEssai"].ToString();
                        client.CodePostalCommerce = oleDbReader["CodePostalEssai"].ToString();
                        client.VilleCommerce = oleDbReader["VilleEssai"].ToString();
                        client.NomCommerce = oleDbReader["NomDuCommerce"].ToString();
                        string initialPartenaire = oleDbReader["InitialesPartenaire"].ToString();
                        string numeroContrat = oleDbReader["NumContrat"].ToString();
                        if (initialPartenaire != "" && numeroContrat != "")
                            client.IdContrat = initialPartenaire + numeroContrat;
                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                //Fermeture du lecteur
                oleDbReader.Close();
                //Fermeture de la connection
                oleDbConnection_BCP.Close();
            }

            return client;
        }

        public void update(Client client)
        {
            string query = "UPDATE tblessaicontrat "
                         + "SET NomDuCommerce = ? , NomClientEssai = ? , PrenomClientEssai = ? , AdresseEssai = ? , VilleEssai = ? , CodePostalEssai = ? , IntituleClientEssai = ? "
                         + "WHERE N°Essai = ? ";

            //Objet permettant de préparer une commande SQL sur une DB MS Access                        
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@NomDuCommerce", client.NomCommerce);
            oleDbCommand.Parameters.AddWithValue("@NomClientEssai", client.Nom);
            oleDbCommand.Parameters.AddWithValue("@PrenomClientEssai", client.Prenom);
            oleDbCommand.Parameters.AddWithValue("@AdresseEssai", client.AdresseCommerce);
            oleDbCommand.Parameters.AddWithValue("@VilleEssai", client.VilleCommerce);
            oleDbCommand.Parameters.AddWithValue("@CodePostalEssai", client.CodePostalCommerce);
            oleDbCommand.Parameters.AddWithValue("@IntituleClientEssai", client.Civilite);
            oleDbCommand.Parameters.AddWithValue("@N°Essai", client.Id);

            //Ouverture de la connection
            oleDbConnection_BCP.Open();

            try
            {
                //Exécution du query
                oleDbCommand.ExecuteNonQuery();
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                //Fermeture de la connection
                oleDbConnection_BCP.Close();
            }
        }


        public long add(Client client)
        {
            //Id retourné et initialisé a -1 car si pas modifié, -1 est le code d'erreur
            long newId = -1;

            string query = "INSERT INTO tblessaicontrat "
                         + "( NomDuCommerce , NomClientEssai , PrenomClientEssai , AdresseEssai , VilleEssai, CodePostalEssai , IntituleClientEssai ) "
                         + "VALUES ( ? , ? , ? , ? , ? , ? , ? )";

            //Objet permettant de préparer une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@NomDuCommerce", client.NomCommerce);
            oleDbCommand.Parameters.AddWithValue("@NomClientEssai", client.Nom);
            oleDbCommand.Parameters.AddWithValue("@PrenomClientEssai", client.Prenom);
            oleDbCommand.Parameters.AddWithValue("@AdresseEssai", client.AdresseCommerce);
            oleDbCommand.Parameters.AddWithValue("@VilleEssai", client.VilleCommerce);
            oleDbCommand.Parameters.AddWithValue("@CodePostalEssai", client.CodePostalCommerce);
            oleDbCommand.Parameters.AddWithValue("@IntituleClientEssai", client.Civilite);

            //Ouverture de la connection
            oleDbConnection_BCP.Open();

            try
            {
                //Exécution du query
                oleDbCommand.ExecuteNonQuery();

                //Pour récupéré le dernier ID écrit en db
                query = "SELECT @@IDENTITY";
                oleDbCommand.CommandText = query;
                newId = (int)oleDbCommand.ExecuteScalar();
            }
            catch (System.Exception)
            {

                throw;
            }
            finally
            {
                //Fermeture de la connection
                oleDbConnection_BCP.Close();
            }

            return newId;
        }
    }
}
