﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;

///
/// Auteur : Burléon Junior
/// Date de création : 12/02/2014
/// 

namespace BiocoldProcess.Dao
{
    /// <summary>
    /// Cette classe permet de manager la table tblAgent avec les DB MS Access
    /// Les informations pouvant être manager ne sont que celles définient par la classe Agent
    /// <seealso cref="Domaine.Agent"/>
    /// </summary>
    public class AgentDao : IAgentDao
    {
        /**
         * Pour récupérer la connection à la DB MS Access BCP
         * */
        private OleDbConnection oleDbConnection_BCP = ConnectionBCP.getConnection();

        /**
         * OleDateReader est un objet récupérant les informations retourné par la requête. Cette objet est en lecture seul.
         * */
        OleDbDataReader oleDbReader = null;

        public Agent find(string initial)
        {
            throw new NotImplementedException();
        }

        public Agent find(int idUser)
        {
            Agent agent = null;

            //Query Sql
            string query = "SELECT * "
                         + "FROM tblAgent "
                         + "WHERE id_util = ? ";

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@id_util", idUser);

            //Ouverture de la connection
            oleDbConnection_BCP.Open();

            try
            {
                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                //Si on a bien récupéré un record on hydrate un objet Partenaire
                while (oleDbReader.Read())
                {
                    if (oleDbReader["id_util"].ToString() != null)
                    {
                        agent = new Agent();
                        agent.Id = oleDbReader["InitialesAgent"].ToString();
                        agent.Nom = oleDbReader["NomAgent"].ToString();
                        agent.Prenom = oleDbReader["PrénomAgent"].ToString();
                        agent.Telephone = oleDbReader["TélAgent"].ToString();
                        agent.TelephonePortable = oleDbReader["TélPortAgent"].ToString();
                        agent.IdPartenaire = oleDbReader["InitPart"].ToString();
                        if (oleDbReader["DateEntreeAgent"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                            agent.DateEntree = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateEntreeAgent"));
                        if (oleDbReader["DateSortieAgent"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                            agent.DateSortie = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DateSortieAgent"));
                        if (oleDbReader["AgentActif"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                            agent.Actif = oleDbReader.GetBoolean(oleDbReader.GetOrdinal("AgentActif"));
                        if (oleDbReader["id_util"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                            agent.IdUtilisateur = oleDbReader.GetInt32(oleDbReader.GetOrdinal("id_util"));

                    }
                }
            }
            catch (System.Exception)
            {
                throw;
            }
            finally
            {
                //Fermeture du lecteur
                oleDbReader.Close();
                //Fermeture de la connection
                oleDbConnection_BCP.Close();
            }

            return agent;
        }

        public void delete(string initial)
        {
            throw new NotImplementedException();
        }

        public void add(Agent agent)
        {
            throw new NotImplementedException();
        }

        public void update(Agent agent)
        {
            throw new NotImplementedException();
        }

        public List<Agent> list()
        {
            throw new NotImplementedException();
        }
    }
}