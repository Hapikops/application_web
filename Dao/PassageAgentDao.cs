﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using BiocoldProcess.Domaine;
using BiocoldProcess.IDao;

///
/// Auteur : Burléon Junior
/// Date de création : 05/02/2014
/// 

namespace BiocoldProcess.Dao
{
    /// <summary>
    /// Cette classe permet de manager la table tblPassageAgent avec les DB MS Access
    /// <seealso cref="Domaine.PassageAgentWS"/>
    /// </summary>
    public class PassageAgentDao : IPassageAgentDao
    {
        /**
         * Pour récupérer la connection à la DB MS Access BCP
         * */
        private OleDbConnection oleDbConnection_BCP = ConnectionBCP.getConnection();

        /**
         * OleDateReader est un objet récupérant les informations retourné par la requête. Cette objet est en lecture seul.
         * */
        OleDbDataReader oleDbReader = null;


        public long add(PassageAgent passageAgent)
        {
            //Id retourné et initialisé a -1 car si pas modifié, -1 est le code d'erreur
            long newId = -1;

            string query = "INSERT INTO tblPassageAgent "
                         + "(N°ChgtPart,N°Essai,InitialesAgent,TypePassage,DatePassage,DateChgtK7,ObsPassInt,ObsPassClt,DateSaisieChgt,IdUtilSaisie,heurePass,duree_pass,vis_vrc,result_vrc,obs_refus) "
                         + "VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? )";

            //Objet permettant de préparer une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@N°ChgtPart", passageAgent.NombrePassage);
            oleDbCommand.Parameters.AddWithValue("@N°Essai", passageAgent.IdEssai);
            oleDbCommand.Parameters.AddWithValue("@InitialesAgent", passageAgent.IdAgent);
            oleDbCommand.Parameters.AddWithValue("@TypePassage", passageAgent.TypePassage);
            oleDbCommand.Parameters.AddWithValue("@DatePassage", passageAgent.DatePassage);
            oleDbCommand.Parameters.AddWithValue("@DateChgtK7", passageAgent.DateChangementK7);
            oleDbCommand.Parameters.AddWithValue("@ObsPassInt", passageAgent.ObservationPassageInterne);
            oleDbCommand.Parameters.AddWithValue("@ObsPassClt", passageAgent.ObservationPassageClient);
            oleDbCommand.Parameters.AddWithValue("@DateSaisieChgt", passageAgent.DateSaisieChangement);
            oleDbCommand.Parameters.AddWithValue("@IdUtilSaisie", passageAgent.IdUtilisateur);
            oleDbCommand.Parameters.AddWithValue("@heurePass", passageAgent.HeurePassage);
            oleDbCommand.Parameters.AddWithValue("@duree_pass", passageAgent.DureePassage);
            oleDbCommand.Parameters.AddWithValue("@vis_vrc", passageAgent.VisiteResolutionContrat);
            oleDbCommand.Parameters.AddWithValue("@result_vrc", passageAgent.ResultatResolutionContrat);
            oleDbCommand.Parameters.AddWithValue("@obs_refus", passageAgent.ObservationRefus);

            //Ouverture de la connection
            oleDbConnection_BCP.Open();

            try
            {
                //Exécution du query
                oleDbCommand.ExecuteNonQuery();

                //Pour récupéré le dernier ID écrit en db
                query = "SELECT @@IDENTITY";
                oleDbCommand.CommandText = query;
                newId = (int)oleDbCommand.ExecuteScalar();
            }
            catch (System.Exception)
            {

                throw;
            }
            finally
            {

                //Fermeture de la connection
                oleDbConnection_BCP.Close();
            }

            return newId;

        }

        public List<PassageAgent> list()
        {
            List<PassageAgent> listPassage = null;

            //Query Sql
            string query = "SELECT TOP 20 NumChgt,  N°Essai, InitialesAgent, DatePassage, IdUtilSaisie, TypePassage "
                         + "FROM tblPassageAgent "
                         + "ORDER BY NumChgt DESC";


            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);

            //Ouverture de la connection
            oleDbConnection_BCP.Open();

            try
            {
                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                listPassage = new List<PassageAgent>();

                //Si on a bien récupéré un record on hydrate un objet Partenaire
                while (oleDbReader.Read())
                {
                    PassageAgent passage = new PassageAgent();
                    passage.IdAgent = oleDbReader["InitialesAgent"].ToString();
                    passage.TypePassage = oleDbReader["TypePassage"].ToString();
                    if (oleDbReader["IdUtilSaisie"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        passage.IdUtilisateur = oleDbReader.GetInt16(oleDbReader.GetOrdinal("IdUtilSaisie"));
                    if (oleDbReader["NumChgt"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        passage.Id = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumChgt"));
                    if (oleDbReader["DatePassage"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        passage.DatePassage = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DatePassage"));
                    if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        passage.IdEssai = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));

                    listPassage.Add(passage);
                }
            }
            catch (System.Exception)
            {
                listPassage = null;
                throw;
            }
            finally
            {
                //Fermeture du lecteur
                if (oleDbReader != null)
                    oleDbReader.Close();
                //Fermeture de la connection
                if (oleDbConnection_BCP != null)
                    oleDbConnection_BCP.Close();
            }
            return listPassage;
        }

        public List<PassageAgent> list(string idAgent)
        {
            List<PassageAgent> listPassage = null;

            //Query Sql
            string query = "SELECT tblPassageAgent.NumChgt,  tblPassageAgent.N°Essai, tblPassageAgent.DatePassage, tblPassageAgent.TypePassage, tblessaicontrat.InitialesPartenaire, tblessaicontrat.NumContrat, tblessaicontrat.NumEssaiPart "
                         + "FROM tblPassageAgent "
                         + "INNER JOIN tblessaicontrat "
                         + "ON tblPassageAgent.N°Essai = tblessaicontrat.N°Essai "
                         + "WHERE tblPassageAgent.InitialesAgent = ? AND tblPassageAgent.DatePassage >= ? "
                         + "ORDER BY tblPassageAgent.DatePassage DESC ";

            //Objet permettant d'exécuter une commande SQL sur une DB MS Access
            OleDbCommand oleDbCommand = new OleDbCommand(query, oleDbConnection_BCP);
            oleDbCommand.Parameters.AddWithValue("@InitialesAgent", idAgent);
            oleDbCommand.Parameters.AddWithValue("@DatePassage", DateTime.Now.AddDays(-7).ToString());

            //Ouverture de la connection
            oleDbConnection_BCP.Open();

            try
            {
                //Exécution du Query
                oleDbReader = oleDbCommand.ExecuteReader();

                listPassage = new List<PassageAgent>();

                //Si on a bien récupéré un record on hydrate un objet Partenaire
                while (oleDbReader.Read())
                {
                    Contrat contrat = new Contrat();
                    PassageAgent passage = new PassageAgent();
                    passage.TypePassage = oleDbReader["TypePassage"].ToString();
                    if (oleDbReader["NumChgt"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        passage.Id = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumChgt"));
                    if (oleDbReader["DatePassage"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        passage.DatePassage = oleDbReader.GetDateTime(oleDbReader.GetOrdinal("DatePassage"));
                    if (oleDbReader["N°Essai"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        passage.IdEssai = oleDbReader.GetInt32(oleDbReader.GetOrdinal("N°Essai"));
                    if (oleDbReader["NumEssaiPart"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG
                        passage.IdEssaiPartenaire = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumEssaiPart"));

                    if (oleDbReader["NumContrat"].ToString() != "") // Si le champ est vide on exécute pas le cast sinon BUG ET Si le champ est vide c'est que se n'est pas un contrat donc on exécute pas la suite
                    {
                        contrat.NumeroContrat = oleDbReader.GetInt32(oleDbReader.GetOrdinal("NumContrat"));
                        if (contrat.NumeroContrat != 0)
                        {
                            contrat.IdPartenaire = oleDbReader["InitialesPartenaire"].ToString();
                            passage.IdContrat = contrat.Id;
                        }
                        else
                            passage.IdContrat = null;

                        
                    }

                    listPassage.Add(passage);
                }
            }
            catch (System.Exception)
            {
                listPassage = null;
                throw;
            }
            finally
            {
                //Fermeture du lecteur
                if (oleDbReader != null)
                    oleDbReader.Close();
                //Fermeture de la connection
                if (oleDbConnection_BCP != null)
                    oleDbConnection_BCP.Close();
            }
            return listPassage;
        }
    }
}