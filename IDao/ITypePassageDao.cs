﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BiocoldProcess.Domaine;

///
///Auteur : frederic radigoy
///Date de creation : 17/03/2014
///

namespace BiocoldProcess.IDao
{
    /// <summary>
    /// Definit ce que doit faire TypePassageDao
    /// </summary>
    interface ITypePassageDao
    {
        /// <summary>
        /// Cette methode permet d'ajouter un type de passage
        /// </summary>
        /// <param name="typeCommerce">le type de passage a ajouté</param>
        void add(TypePassage typePassage);

        /// <summary>
        /// Cette methode permet de supprimer un type de passage
        /// </summary>
        /// <param name="id">le type de passage a supprimé</param>
        void delete(int id);

        /// <summary>
        /// cette méthode permet de mettre à jour un type de passage
        /// </summary>
        /// <param name="typeCommerce">le type de passage a mettre à jour</param>
        void update(TypePassage typePassage);

        /// <summary>
        /// Cette méthode permet de lister les types de passage
        /// </summary>
        /// <returns>liste de type passage</returns>
        List<TypePassage> list();
    }
}