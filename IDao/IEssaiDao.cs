﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 21/03/2014
/// 

namespace BiocoldProcess.IDao
{
    /// <summary>
    /// Défini se que doit faire EssaiDao
    /// </summary>
    interface IEssaiDao
    {
        /// <summary>
        /// Méthode permetant de mettre à jour un essai
        /// </summary>
        /// <param name="essai">L'essai à mettre à jour</param>
        void update(Essai essai);

        /// <summary>
        /// Permet de récupérer une liste d'essai en fonction de l'ID d'un partenaire.
        /// /!\ Attention /!\ Seul les essais n'ayant pas de contrat sont sélectionné 
        /// </summary>
        /// <param name="idPartenaire">Id du partenaire sur lequel sera sélectionné la liste d'essai.</param>
        /// <returns>Retourne une liste d'essai n'ayant pas de contrat.</returns>
        List<Essai> listWithoutContract(string idPartenaire);

        /// <summary>
        /// Méthode permetant de récupérer les informations sur un essai.
        /// </summary>
        /// <param name="id">L'id d'un essai permet d'identifier un essai</param>
        /// <returns>Retourne un Essai avec uniquement les informations que l'objet Essai peut stocker.</returns>
        Essai find(int id);

        /// <summary>
        /// Cette méthode permet de récupérer le dernier "numéro d'essai partenaire" d'un partenaire. 
        /// </summary>
        /// <param name="idPartenaire">Id du partenaire en fonction duquel on doit donné le dernier numéro d'essai partenaire.</param>
        /// <returns>Retourne le dernier numéro d'essai partenaire. Attention ce numéro vaut 0 si une erreur se produit ou que le partenaire n'existe pas ou n'a pas de précédent essai.</returns>
        int lastNumEssaiPart(string idPartenaire);
    }
}
