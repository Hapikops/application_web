﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 12/02/2014
/// 

namespace BiocoldProcess.IDao
{
    /// <summary>
    /// Défini se que doit faire ClientDao
    /// </summary>
    interface IClientDao
    {
        /// <summary>
        /// Méthode permetant de récupérer les informations sur un client.
        /// </summary>
        /// <param name="id">C'est par l'id qu'est identifié un client. </param>
        /// <returns>Retourne un Client avec uniquement les informations que l'objet Client peut stocker.</returns>
        Client find(int id);

        /// <summary>
        /// Méthode permetant de mettre à jour un client
        /// </summary>
        /// <param name="client">Le client mis à jour</param>
        void update(Client client);

        /// <summary>
        /// Permet d'inserer un client en db
        /// </summary>
        /// <param name="client">Un objet client</param>
        /// <returns>
        /// Retourne l'id du dernier client écrit en db 
        /// Si une erreur a lieu, ça retourne -1
        /// </returns>
        long add(Client client);

    }
}
