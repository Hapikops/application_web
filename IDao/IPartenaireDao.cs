﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 30/01/2014
/// 

namespace BiocoldProcess.IDao
{
    /// <summary>
    /// Défini se que doit faire PartenaireDao
    /// </summary>
    interface IPartenaireDao
    {
        /// <summary>
        /// Méthode permetant de récupérer les informations sur un partenaire.
        /// </summary>
        /// <param name="initial">C'est par les id qu'est identifié un partenaire. </param>
        /// <returns>Retourne un Partenaire avec uniquement les informations que l'objet Partenaire peut stocker.</returns>
        Partenaire find(string initial);

        /// <summary>
        /// Méthode permetant de supprimer un partenaire
        /// </summary>
        /// <param name="initial">C'est par les id qu'est identifié un partenaire. </param>
        void delete(string initial);

        /// <summary>
        /// Méthode permetant d'ajouter un partenaire
        /// </summary>
        /// <param name="partenaire">Le partenaire à ajouter</param>
        void add(Partenaire partenaire);

        /// <summary>
        /// Méthode permetant de mettre à jour un partenaire
        /// </summary>
        /// <param name="partenaire">Le partenaire à mettre à jour</param>
        void update(Partenaire partenaire);

        /// <summary>
        /// Méthode permetant de récupérer une liste des partenaires
        /// </summary>
        /// <returns>Retourne une liste de partenaire. </returns>
        List<Partenaire> list();
    }
}
    