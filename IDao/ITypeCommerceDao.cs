﻿using BiocoldProcess.Domaine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

///
///Auteur : Burléon Junior
///Date de creation : 03/04/2014
///

namespace BiocoldProcess.IDao
{
    /// <summary>
    /// Definit ce que doit faire TypeCommerceDao
    /// </summary>
    interface ITypeCommerceDao
    {
        /// <summary>
        /// Cette méthode permet de lister les types de commerce
        /// </summary>
        /// <returns>Liste de type de commerce</returns>
        List<TypeCommerce> list();
    }
}
