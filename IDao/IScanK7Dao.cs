﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 04/02/2014
/// 
/// Auteur de Modification : frederic Radigoy
/// Date de Modification : 03/03/2014
/// 

namespace BiocoldProcess.IDao
{
    /// <summary>
    /// Défini se que doit faire ScanDao
    /// </summary>
    interface IScanK7Dao
    {
        /// <summary>
        /// Méthode permetant d'ajouter un scanK7
        /// </summary>
        /// <param name="scanK7">Un objet ScanK7</param>
        void add(ScanK7 scanK7);

        /// <summary>
        /// Permet de récupérer une liste de contrat en fonction de l'ID d'un partenaire.
        /// </summary>
        /// <param name="idPartenaire">Id du partenaire sur lequel sera sélectionné la liste de contrat.</param>
        /// <returns>Retourne une liste de contrat.</returns>
        List<ScanK7> list(int idUtilisateur);
        
        /// <summary>
        /// Permet de récupérer une liste de contrat en fonction de l'ID d'un partenaire et de la date de saisie
        /// </summary>
        /// <param name="idUtilisateur">IdPartenaire</param>
        /// <param name="dateSaisie">dateSaisie</param>
        /// <returns>Retourne une liste de contrat</returns>
        List<ScanK7> listScan(int idUtilisateur, DateTime dateSaisie);
        /// <summary>
        /// Permet de récuperer une liste de contrat 
        /// </summary>
        /// <returns>Retourne une dliste de contrat</returns>
        List<ScanK7> liste();
    }
}
