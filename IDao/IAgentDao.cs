﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 12/02/2014
/// 

namespace BiocoldProcess.IDao
{
    /// <summary>
    /// Défini se que doit faire AgentDao
    /// </summary>
    interface IAgentDao
    {
        /// <summary>
        /// Méthode permetant de récupérer les informations sur un agent.
        /// </summary>
        /// <param name="initial">C'est par les initial qu'est identifié un agent. </param>
        /// <returns>Retourne un Agent avec uniquement les informations que l'objet Agent peut stocker.</returns>
        Agent find(string initial);

        /// <summary>
        /// Méthode permetant de récupérer les informations sur un agent.
        /// </summary>
        /// <param name="idUser">C'est par l'id utilisateur dans ce cas-ci que l'on trouvera l'agent. </param>
        /// <returns>Retourne un Agent avec uniquement les informations que l'objet Agent peut stocker.</returns>
        Agent find(int idUser);

        /// <summary>
        /// Méthode permetant de supprimer un agent
        /// </summary>
        /// <param name="initial">C'est par l'id qu'est identifié un agent. </param>
        void delete(string initial);

        /// <summary>
        /// Méthode permetant d'ajouter un agent
        /// </summary>
        /// <param name="agent">L'agent à ajouter</param>
        void add(Agent agent);

        /// <summary>
        /// Méthode permetant de mettre à jour un agent
        /// </summary>
        /// <param name="agent">L'agent à mettre à jour</param>
        void update(Agent agent);

        /// <summary>
        /// Méthode permetant de récupérer une liste des agents
        /// </summary>
        /// <returns>Retourne une liste d'agent. </returns>
        List<Agent> list();
    }
}
