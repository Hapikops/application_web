﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 11/02/2014
/// 

namespace BiocoldProcess.IDao
{
    /// <summary>
    /// Défini se que doit faire ContratDao
    /// </summary>
    interface IContratDao
    {
        /// <summary>
        /// Méthode permetant de récupérer les informations sur un contrat.
        /// </summary>
        /// <param name="id">C'est par l'id qu'est identifié un contrat. </param>
        /// <returns>Retourne un Contrat avec uniquement les informations que l'objet Contrat peut stocker.</returns>
        Contrat find(string id);

        /// <summary>
        /// Méthode permetant de récupérer les informations sur un contrat.
        /// </summary>
        /// <param name="idEssai">L'id d'un essai permet d'identifier un contrat si un contrat existe pour cette essai </param>
        /// <returns>Retourne un Contrat avec uniquement les informations que l'objet Contrat peut stocker.</returns>
        Contrat find(int idEssai);

        /// <summary>
        /// Méthode permetant de mettre à jour un contrat
        /// </summary>
        /// <param name="contrat">Le contrat à mettre à jour</param>
        void update(Contrat contrat);

        /// <summary>
        /// Méthode permetant de récupérer une liste des contrats
        /// </summary>
        /// <returns>Retourne une liste de contrat. </returns>
        List<Contrat> list();

        /// <summary>
        /// Permet de récupérer une liste de contrat en fonction de l'ID d'un partenaire.
        /// </summary>
        /// <param name="idPartenaire">Id du partenaire sur lequel sera sélectionné la liste de contrat.</param>
        /// <returns>Retourne une liste de contrat.</returns>
        List<Contrat> list(string idPartenaire);
    }
}
