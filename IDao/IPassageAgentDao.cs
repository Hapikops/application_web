﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 05/02/2014
/// 

namespace BiocoldProcess.IDao
{
    /// <summary>
    /// Défini se que doit faire PassageAgentDao
    /// </summary>
    interface IPassageAgentDao
    {
        /// <summary>
        /// Permet d'ajouter un passage d'agent à la db
        /// </summary>
        /// <param name="passageAgent">Un objet passage agent</param>
        /// <returns>
        /// Retourne l'id du dernier passageAgent écrit en db 
        /// Si une erreur a lieu, ça retourne -1
        /// </returns>
        long add(PassageAgent passageAgent);

        /// <summary>
        /// Méthode permetant de récupérer une liste des passageAgents
        /// </summary>
        /// <returns>Retourne une liste des 20 derniers passageAgent. </returns>
        List<PassageAgent> list();

        /// <summary>
        /// Méthode permetant de récupérer une liste des passageAgents
        /// </summary>
        /// <returns>Retourne une liste des passage en fonction de l'agent qui les a fait. Attention cette liste ne contient que les passage des 7 derniers jours ! </returns>
        List<PassageAgent> list(string idAgent);
    }
}