﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BiocoldProcess.Domaine;

///
/// Auteur : Burléon Junior
/// Date de création : 03/02/2014
/// 

namespace BiocoldProcess.IDao
{
    /// <summary>
    /// Défini se que doit faire UtilisateurDao
    /// </summary>
    interface IUtilisateurDao
    {
        /// <summary>
        /// Méthode permetant de récupérer les informations sur un Utilisateur.
        /// </summary>
        /// <param name="login">C'est grâce au login que l'on retrouve un utilisateur. </param>
        /// <returns>Retourne un Utilisateur.</returns>
        Utilisateur find(string login);

        /// Méthode permetant de savoir si un utilisateur existe.
        /// </summary>
        /// <param name="login">Login de l'utilisateur dont on veut savoir si il existe</param>
        /// <returns>
        /// Vrai : Il existe
        /// Faux : Il n'existe pas
        /// </returns>
        bool exist(string login);

        /// <summary>
        /// Méthode permetant de récupérer les informations sur un Utilisateur.
        /// </summary>
        /// <param name="id">C'est grâce à l'id que l'on retrouve un utilisateur. </param>
        /// <returns>Retourne un Utilisateur.</returns>
        Utilisateur find(int id);

        /// <summary>
        /// Méthode permetant de supprimer un Utilisateur
        /// </summary>
        /// <param name="id">C'est par les id qu'est identifié un Utilisateur. </param>
        void delete(int id);

        /// <summary>
        /// Méthode permetant d'ajouter un Utilisateur
        /// </summary>
        /// <param name="Utilisateur">L'utilisateur à ajouter</param>
        void add(Utilisateur utilisateur);

        /// <summary>
        /// Méthode permetant de mettre à jour un Utilisateur
        /// </summary>
        /// <param name="Utilisateur">L'utilisateur à mettre à jour</param>
        void update(Utilisateur utilisateur);

        /// <summary>
        /// Méthode permetant de récupérer une liste des utilisateurs
        /// </summary>
        /// <returns>Retourne une liste d'utilisateur. </returns>
        List<Utilisateur> list();
    }
}
